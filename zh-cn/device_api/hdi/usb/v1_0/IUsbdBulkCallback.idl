/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup USB
 * @{
 *
 * @brief 提供统一的USB驱动标准接口，实现USB驱动接入。
 *
 * 上层USB服务开发人员可以根据USB驱动模块提供的标准接口获取如下功能：打开/关闭设备，获取设备描述符，获取文件描述符，打开/关闭接口，批量读取/写入数据，
 * 设置/获取设备功能，绑定/解绑订阅者等。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IUsbdBulkCallback.idl
 *
 * @brief USB驱动批量传输读/写数据的回调。
 *
 * 模块包路径：ohos.hdi.usb.v1_0
 *
 * @since 3.2
 * @version 1.0
 */

package ohos.hdi.usb.v1_0;

/**
 * @brief USB驱动的回调函数。
 *
 * 当USB驱动进行批量传输异步读/写数据时调用回调函数，处理对应的结果。
 *
 * @since 3.2
 * @version 1.0
 */
[callback] interface IUsbdBulkCallback {
    /**
     * @brief 批量写数据的回调函数。
     *
     * @param status 完成状态。
     * @param actLength 写数据时实际发送的数据长度。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    OnBulkWriteCallback([in] int status, [in] int actLength);

    /**
     * @brief 批量读数据的回调函数。
     *
     * @param status 完成状态。
     * @param actLength 读数据时实际接收的数据长度。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    OnBulkReadCallback([in] int status, [in] int actLength);
}
/** @} */
