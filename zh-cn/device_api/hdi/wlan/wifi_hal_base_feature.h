/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup WLAN
 * @{
 *
 * @brief WLAN模块向上层WLAN服务提供了统一接口。
 *
 * HDI层开发人员可根据WLAN模块提供的向上统一接口获取如下能力：建立/关闭WLAN热点，扫描，关联WLAN热点，WLAN平台芯片管理，网络数据缓冲的申请、释放、移动等操作，网络设备管理，电源管理等。
 *
 * @since 1.0
 * @version 1.0
 */

/**
 * @file wifi_hal_base_feature.h
 *
 * @brief 提供WLAN基本特性能力。
 *
 * @since 1.0
 * @version 1.0
 */

#ifndef WIFI_HAL_BASE_FEATURE_H
#define WIFI_HAL_BASE_FEATURE_H

#include <stdint.h>

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif

/**
 * @brief 网卡名称最大长度。
 */
#define IFNAME_MAX_LEN 16
/**
 * @brief WLAN的MAC地址长度。
 */
#define WIFI_MAC_ADDR_LENGTH 6
/**
 * @brief 定义访问失败错误码。
 */
#define ERR_UNAUTH_ACCESS (-6)

/**
 * @brief 枚举WLAN相关特性的类型{@link FeatureType}。
 *
 * @version 1.0
 */
typedef enum {
    /**< 未定义的类型 */
    PROTOCOL_80211_IFTYPE_UNSPECIFIED,
    /**< 特设型网络 */
    PROTOCOL_80211_IFTYPE_ADHOC,
    /**< 工作站 */
    PROTOCOL_80211_IFTYPE_STATION,
    /**< 接入点 */
    PROTOCOL_80211_IFTYPE_AP,
    /**< 虚拟接入点 */
    PROTOCOL_80211_IFTYPE_AP_VLAN,
    /**< 无线分布式系统 */
    PROTOCOL_80211_IFTYPE_WDS,
    /**< 网络监听器 */
    PROTOCOL_80211_IFTYPE_MONITOR,
    /**< 组网 */
    PROTOCOL_80211_IFTYPE_MESH_POINT,
    /**< 对等网络客户端 */
    PROTOCOL_80211_IFTYPE_P2P_CLIENT,
    /**< 对等网络群组所有者 */
    PROTOCOL_80211_IFTYPE_P2P_GO,
    /**< 对等网络设备 */
    PROTOCOL_80211_IFTYPE_P2P_DEVICE,
     /**< 网口的数目 */
    PROTOCOL_80211_IFTYPE_NUM,
} FeatureType;

/**
 * @brief WLAN基本特性操作接口，包括获取网卡名称，设置MAC地址，设置发射功率等公共能力接口。
 *
 * @since 1.0
 * @version 1.0
 */
struct IWiFiBaseFeature {
    /**< 网卡名称 */
    char ifName[IFNAME_MAX_LEN];
    /**< 特性的类型{@link FeatureType} */
    int32_t type;

    /**
     * @brief 根据基本特性获取网卡名称。
     *
     * @param baseFeature 输入参数，基本特性{@link IWiFiBaseFeature}。
     *
     * @return 如果操作成功，则返回网卡名称。
     * @return 如果操作失败，则返回NULL。
     *
     * @since 1.0
     * @version 1.0
     */
    const char *(*getNetworkIfaceName)(const struct IWiFiBaseFeature *baseFeature);

    /**
     * @brief 获取基本特性的类型{@link FeatureType}。
     *
     * @param baseFeature 输入参数，基本特性{@link IWiFiBaseFeature}。
     *
     * @return 如果操作成功，则返回特性类型。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*getFeatureType)(const struct IWiFiBaseFeature *baseFeature);

    /**
     * @brief 根据传入参数设置对应网卡的MAC地址。
     *
     * @param baseFeature 输入参数，基本特性{@link IWiFiBaseFeature}。
     * @param mac 输入参数，设置的MAC地址。
     * @param len 输入参数，设置的MAC地址长度。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*setMacAddress)(const struct IWiFiBaseFeature *baseFeature, unsigned char *mac, uint8_t len);

    /**
     * @brief 获取设备的MAC地址。
     *
     * @param baseFeature 输入参数，基本特性{@link IWiFiBaseFeature}。
     * @param mac 输出参数，获得的MAC地址。
     * @param len 输入参数，获得的MAC地址长度。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*getDeviceMacAddress)(const struct IWiFiBaseFeature *baseFeature, unsigned char *mac, uint8_t len);

    /**
     * @brief 获取指定频段（2.4G或者5G）下支持的频率。
     *
     * @param baseFeature 输入参数，基本特性{@link IWiFiBaseFeature}。
     * @param band 输入参数，指定的一个频段。
     * @param freqs 输出参数，保存支持的频率。
     * @param count 输入参数，频率数组的元素个数。
     * @param num 输出参数，实际支持的频率个数。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*getValidFreqsWithBand)(const struct IWiFiBaseFeature *baseFeature, int32_t band, int32_t *freqs,
        uint32_t count, uint32_t *num);

    /**
     * @brief 设置发射功率。
     *
     * @param baseFeature 输入参数，基本特性{@link IWiFiBaseFeature}。
     * @param power 输入参数，设置的发射功率。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*setTxPower)(const struct IWiFiBaseFeature *baseFeature, int32_t power);

    /**
     * @brief 获得当前驱动的芯片ID。
     *
     * @param baseFeature 输入参数，基本特性{@link IWiFiBaseFeature}。
     * @param chipId 输出参数，获得的芯片ID。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*getChipId)(const struct IWiFiBaseFeature *baseFeature, uint8_t *chipId);

    /**
     * @brief 通过芯片ID获得当前芯片所有的网卡名称。
     *
     * @param chipId 输入参数，需要获取网卡名称的芯片ID。
     * @param ifNames 输出参数，网卡名称。
     * @param num 输出参数，网卡的数量。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*getIfNamesByChipId)(const uint8_t chipId, char **ifNames, uint32_t *num);
};

/**
 * @brief WLAN服务创建任何类型的特性{@Link FeatureType}时，都需要调用此函数。
 *
 * @param fe 输入参数，基本特性{@link IWiFiBaseFeature}。
 *
 * @return 如果操作成功，则返回0。
 * @return 如果操作失败，则返回负值。
 *
 * @since 1.0
 * @version 1.0
 */
int32_t InitBaseFeature(struct IWiFiBaseFeature **fe);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif

#endif
/** @} */
