/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiDrm
 * @{
 * @brief DRM模块接口定义。
 *
 * DRM是数字版权管理，用于对多媒体内容加密，以便保护价值内容不被非法获取，DRM模块接口定义了DRM实例管理、DRM会话管理、DRM内容解密的接口。
 *
 * @since 4.1
 * @version 1.0
 */

/**
 * @file MediaKeySystemTypes.idl
 *
 * @brief 定义了HdiDrm使用的类型及结构。
 *
 * 模块包路径：ohos.hdi.drm.v1_0
 *
 * @since 4.1
 * @version 1.0
 */

package ohos.hdi.drm.v1_0;

/**
 * @brief 内容保护等级。
 *
 * @since 4.1
 * @version 1.0
 */
enum ContentProtectionLevel {
    /**
     * 未知等级。
     */
    SECURE_UNKNOWN = 0,
    /**
     * 软件安全等级。
     */
    SW_SECURE_CRYPTO,
    /**
     * 硬件安全等级。
     */
    HW_SECURE_CRYPTO,
    /**
     * 增强硬件安全等级。
     */
    HW_ENHANCED_SECURE_CRYPTO,
    /**
     * 安全等级最大值，用于判断非法安全等级。
     */
    HW_SECURE_MAX,
};

/**
 * @brief 许可证请求类型。
 *
 * @since 4.1
 * @version 1.0
 */
enum MediaKeyRequestType {
    /**
     * 未知类型。
     */
    MEDIA_KEY_REQUEST_TYPE_UNKNOWN = 0,
    /**
     * 初次请求。
     */
    MEDIA_KEY_REQUEST_TYPE_INITIAL,
    /**
     * 再次许可证请求。
     */
    MEDIA_KEY_REQUEST_TYPE_RENEWAL,
    /**
     * 释放许可证请求。
     */
    MEDIA_KEY_REQUEST_TYPE_RELEASE,
    /**
     * 无类型。
     */
    MEDIA_KEY_REQUEST_TYPE_NONE,
    /**
     * 许可证更新请求。
     */
    MEDIA_KEY_REQUEST_TYPE_UPDATE,
};

/**
 * @brief DRM插件监听事件类型。
 *
 * @since 4.1
 * @version 1.0
 */
enum EventType {
    /**
     * 证书下载。
     */
    EVENTTYPE_PROVISIONREQUIRED = 0,
    /**
     * 许可证获取。
     */
    EVENTTYPE_KEYREQUIRED,
    /**
     * 许可证过期。
     */
    EVENTTYPE_KEYEXPIRED,
    /**
     * 厂商自定义事件。
     */
    EVENTTYPE_VENDOR_DEFINED,
    /**
     * 许可证有效期更新。
     */
    EVENTTYPE_EXPIRATIONUPDATE,
    /**
     * 许可证变更。
     */
    EVENTTYPE_KEYCHANGE,
};

/**
 * @brief 加密算法类型。
 *
 * @since 4.1
 * @version 1.0
 */
enum CryptoAlgorithmType {
    /**
     * 未加密。
     */
    ALGTYPE_UNENCRYPTED = 0,
    /**
     * aes_ctr加密。
     */
    ALGTYPE_AES_CTR,
    /**
     * aes_wv加密。
     */
    ALGTYPE_AES_WV,
    /**
     * aes_cbc加密。
     */
    ALGTYPE_AES_CBC,
    /**
     * sm4_cbc加密。
     */
    ALGTYPE_SM4_CBC,
    /**
     * sm4_ctr加密。
     */
    ALGTYPE_SM4_CTR,
};

/**
 * @brief 离线许可证状态。
 *
 * @since 4.1
 * @version 1.0
 */
enum OfflineMediaKeyStatus {
    /**
     * 未知状态。
     */
    OFFLINE_MEDIA_KEY_STATUS_UNKNOWN = 0,
    /**
     * 许可证可用。
     */
    OFFLINE_MEDIA_KEY_STATUS_USABLE,
    /**
     * 许可证不可用（未在生效时间段内、已过期等）。
     */
    OFFLINE_MEDIA_KEY_STATUS_INACTIVE,
};

/**
 * @brief 许可证类型。
 *
 * @since 4.1
 * @version 1.0
 */
enum MediaKeyType {
    /**
     * 离线许可证。
     */
    MEDIA_KEY_TYPE_OFFLINE = 0,
    /**
     * 在线许可证。
     */
    MEDIA_KEY_TYPE_ONLINE,
};

/**
 * @brief 证书状态。
 *
 * @since 4.1
 * @version 1.0
 */
enum CertificateStatus {
    /**
     * 证书已设置。
     */
    CERT_STATUS_PROVISIONED = 0,
    /**
     * 证书未设置。
     */
    CERT_STATUS_NOT_PROVISIONED,
    /**
     * 证书过期。
     */
    CERT_STATUS_EXPIRED,
    /**
     * 证书无效。
     */
    CERT_STATUS_INVALID,
    /**
     * 获取证书状态失败。
     */
    CERT_STATUS_UNAVAILABLE,
};

/**
 * @brief 会话许可证状态。
 *
 * @since 4.1
 * @version 1.0
 */
enum MediaKeySessionKeyStatus {
    /**
     * 许可证可用。
     */
    MEDIA_KEY_SESSION_KEY_STATUS_USABLE = 0,
    /**
     * 许可证过期。
     */
    MEDIA_KEY_SESSION_KEY_STATUS_EXPIRED,
    /**
     * 该许可证不允许输出。
     */
    MEDIA_KEY_SESSION_KEY_STATUS_OUTPUT_NOT_ALLOWED,
    /**
     * 暂时不可用许可证。
     */
    MEDIA_KEY_SESSION_KEY_STATUS_PENDING,
    /**
     * 许可证内部状态错误。
     */
    MEDIA_KEY_SESSION_KEY_STATUS_INTERNAL_ERROR,
    /**
     * 许可证待生效。
     */
    MEDIA_KEY_SESSION_KEY_STATUS_USABLE_IN_FUTURE,
};

/**
 * @brief 定义MediaKeyRequestInfo，该信息由{@link IMediaKeySession::GenerateMediaKeyRequest}使用。
 *
 * @since 4.1
 * @version 1.0
 */
struct MediaKeyRequestInfo {
    /**
     * 许可证类型。
     */
    enum MediaKeyType mediaKeyType;
    /**
     * 媒体类型。
     */
    String mimeType;
    /**
     * 初始信息，从MediaKeySystemInfo中获取。
     */
    unsigned char[] initData;
    /**
     * 应用自定义数据。
     */
    Map<String, String> optionalData;
};

/**
 * @brief 定义MediaKeyRequest，该信息由{@link IMediaKeySession::GenerateMediaKeyRequest}使用。
 *
 * @since 4.1
 * @version 1.0
 */
struct MediaKeyRequest {
    /**
     * 许可证请求的类型。
     */
    enum MediaKeyRequestType requestType;
    /**
     * 许可证请求。
     */
    unsigned char[] data;
    /**
     * 许可证服务器URL。
     */
    String defaultUrl;
};

/**
 * @brief 定义Pattern，该信息由CryptoInfo使用。
 *
 * @since 4.1
 * @version 1.0
 */
struct Pattern {
    /**
     * 加密块数。
     */
    unsigned int encryptBlocks;
    /**
     * 未加密块数。
     */
    unsigned int skipBlocks;
};

/**
 * @brief 定义SubSample，该信息由CryptoInfo使用。
 *
 * @since 4.1
 * @version 1.0
 */
struct SubSample {
    /**
     * 头部长度。
     */
    unsigned int clearHeaderLen;
    /**
     * 负载长度。
     */
    unsigned int payLoadLen;
};

/**
 * @brief 定义CryptoInfo，该信息由{@link IMediaDecryptModule::DecryptMediaData}使用。
 *
 * @since 4.1
 * @version 1.0
 */
struct CryptoInfo {
    /**
     * 加密算法类型。
     */
    enum CryptoAlgorithmType type;
    /**
     * 密钥标识。
     */
    unsigned char[] keyId;
    /**
     * 秘钥配套的IV。
     */
    unsigned char[] iv;
    /**
     * 加密模式。
     */
    struct Pattern pattern;
    /**
     * 加密subsample。
     */
    struct SubSample[] subSamples;
};

/**
 * @brief 定义DrmBuffer，该信息由{@link IMediaDecryptModule::DecryptMediaData}使用。
 *
 * @since 4.1
 * @version 1.0
 */
struct DrmBuffer {
    /**
     * buffer类型，由实现平台定义。
     */
    unsigned int bufferType;
    /**
     * buffer描述符。
     */
    FileDescriptor fd;
    /**
     * buffer长度。
     */
    unsigned int bufferLen;
    /**
     * 分配buffer的长度。
     */
    unsigned int allocLen;
    /**
     * 实际填充数据的长度。
     */
    unsigned int filledLen;
    /**
     * 数据基于buffer首地址的偏移。
     */
    unsigned int offset;
    /**
     * 共享内存类型。
     */
    unsigned int sharedMemType;
};
/** @} */
