/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup NNRt
 * @{
 *
 * @brief NNRt（Neural Network Runtime, 神经网络运行时）是面向AI领域的跨芯片推理计算运行时，作为中间桥梁连通上层AI推理框架和底层加速芯片，实现AI模型的跨芯片推理计算。提供统一AI芯片驱动接口，实现AI芯片驱动接入OpenHarmony。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file NnrtTypes.idl
 *
 * @brief 该文件定义了HDI接口中用到的类型。
 *
 * 模块包路径：ohos.hdi.nnrt.v1_0;
 *
 * @since 3.2
 * @version 1.0
 */

package ohos.hdi.nnrt.v1_0;

/**
 * @brief 共享内存数据的结构体。
 *
 * @since 3.2
 * @version 1.0
 */
struct SharedBuffer {
    /** 共享内存的文件描述符。 */
    FileDescriptor fd;
    /** 共享内存的空间大小，单位字节。 */
    unsigned int bufferSize;
    /** 有效数据起始地址在共享内存中的偏移。 */
    unsigned int offset;
    /** 有效数据的占用空间大小，单位字节。 */
    unsigned int dataSize;
};

/**
 * @brief AI计算芯片的类型。
 *
 * @since 3.2
 * @version 1.0
 */
enum DeviceType: int {
    /** 不属于以下类型的芯片 */
    OTHER,
    /** CPU芯片 */
    CPU,
    /** GPU芯片*/
    GPU,
    /** AI专用加速芯片，比如NPU、DSP*/
    ACCELERATOR
};

/**
 * @brief 用于AI计算芯片的状态。
 *
 * @since 3.2
 * @version 1.0
 */
enum DeviceStatus: int {
    /** 芯片当前处于可用状态。 */
    AVAILABLE,
    /** 芯片当前处于忙碌状态，可能无法及时响应计算任务。 */
    BUSY,
    /** 芯片当前处于下线状态，无法响应计算任务。 */
    OFFLINE,
    /** 芯片当前处于未知状态。 */
    UNKNOWN
};

/**
 * @brief 芯片执行AI计算的性能模式。
 *
 * @since 3.2
 * @version 1.0
 */
enum PerformanceMode: int {
    /** 不指定任何性能模式，具体运行模式由芯片定义。 */
    PERFORMANCE_NONE,
    /** 低性能模式，执行AI计算速度慢，功耗低。 */
    PERFORMANCE_LOW,
    /** 中性能模式，执行AI计算速度较慢，功耗较低。 */
    PERFORMANCE_MEDIUM,
    /** 高性能模式，执行AI计算速度较快，功耗较高。 */
    PERFORMANCE_HIGH,
    /** 最高性能模式，执行AI计算速度快，功耗高。 */
    PERFORMANCE_EXTREME
};

/**
 * @brief AI计算任务的优先级。
 *
 * @since 3.2
 * @version 1.0
 */
enum Priority: int {
    /** 不指定任何任务优先级，具体执行策略由芯片定义。 */
    PRIORITY_NONE,
    /** 低优先级，若有更高优先级的任务，芯片会执行更高优先级的任务。 */
    PRIORITY_LOW,
    /** 中等优先级，若有更高优先级的任务，芯片会执行更高优先级的任务。 */
    PRIORITY_MEDIUM,
    /** 高优先级，高优先级任务最先执行。 */
    PRIORITY_HIGH
};

/**
 * @brief 定义编译模型需要的参数配置。
 *
 * @since 3.2
 * @version 1.0
 */
struct ModelConfig {
    /** float32浮点模型是否以float16浮点运行。 */
    boolean enableFloat16;
    /** 计算任务的性能模式，性能模式定义请查看{@link PerformanceMode}。 */
    enum PerformanceMode mode;
    /** 计算任务的优先级，优先级详情请看{@link Priority}。 */
    enum Priority priority;
};

/**
 * @brief 算子数据排布。需要配合{@link Tensor}使用。
 *
 * @since 3.2
 * @version 1.0
 */
enum Format : byte {
    /** 用于数据排列，作为Format初始的值。 */
    FORMAT_NONE = -1,
    /** 数据排列为NCHW。 */
    FORMAT_NCHW = 0,
    /** 数据排列为NHWC。 */
    FORMAT_NHWC = 1
};

/**
 * @brief 量化参数结构体。
 *
 * 相关的公式如下，q为量化后的参数，r为真实参数，\f$ r_{max} \f$为待量化数据的最大值，\f$ r_{min} \f$待量化数据的最小值，round(x)为x四舍五入取整，clamp(x,min,max)为如下运算：
 \f[
  \text{clamp}(x,min,max) =
  \begin{cases}
       \text{max} & \text{ if } x > \text{ max } \\
       \text{min} & \text{ if } x < \text{ min } \\
       x & \text{ otherwise } \\
   \end{cases}
 \f]
 * 浮点到定点的量化公式：
 \f[
    \text{q}(x_i) = clamp(round(\frac{r}{scale}+zeroPoint), min , max)
 \f]
 * 定点到浮点的反量化公式：
 \f[
    \text{r}= (q-zeroPoint)*scale
 \f]
 * 量化参数scale由如下公式计算：
 \f[
    scale = \frac{r_{max}-r_{min}}{q_{max}-q_{min}}
 \f]
  * 量化参数zeroPoint由如下公式计算：
 \f[
    zeroPoint = round(q_{min}-\frac{r_{min}}{scale})
 \f]
  * 量化参数\f$ q_{min},q_{max} \f$ 如下公式计算：
 \f[
    q_{min} = -(1<<(numBits-1))
 \f]
 \f[
    q_{max} = (1<<(numBits-1))-1
 \f]
 * 特殊情况：当 \f$ r_{min} \f$和 \f$ r_{max} \f$同时为0时，scale 和 zeroPoint均为0。
 *
 * @since 3.2
 * @version 1.0
 */
struct QuantParam {
    /** 量化的bit数 */
    int numBits;
    /** 零值 */
    int zeroPoint;
    /** 量化器的步长 */
    double scale;
};

/**
 * @brief 张量的数据类型。需要配合{@link Tensor}使用。
 *
 * @since 3.2
 * @version 1.0
 */
enum DataType : byte {
    /** 数据类型未知 */
    DATA_TYPE_UNKNOWN = 0,
    /** 数据类型是BOOL */
    DATA_TYPE_BOOL = 30,
    /** 数据类型是INT8 */
    DATA_TYPE_INT8 = 32,
    /** 数据类型是INT16 */
    DATA_TYPE_INT16 = 33,
    /** 数据类型是INT32 */
    DATA_TYPE_INT32 = 34,
    /** 数据类型是INT64 */
    DATA_TYPE_INT64 = 35,
    /** 数据类型是UINT8 */
    DATA_TYPE_UINT8 = 37,
    /** 数据类型是UINT16 */
    DATA_TYPE_UINT16 = 38,
    /** 数据类型是UINT32 */
    DATA_TYPE_UINT32 = 39,
    /** 数据类型是UINT64 */
    DATA_TYPE_UINT64 = 40,
    /** 数据类型是FLOAT16 */
    DATA_TYPE_FLOAT16 = 42,
    /** 数据类型是FLOAT32 */
    DATA_TYPE_FLOAT32 = 43,
    /** 数据类型是FLOAT64 */
    DATA_TYPE_FLOAT64 = 44,
};

/**
 * @brief AI模型的输入输出张量。
 *
 * @since 3.2
 * @version 1.0
 */
struct IOTensor {
    /** 张量的名称。 */
    String name;
    /** 张量的数据类型, 数据类型定义请查看{@link DataType}。*/
    enum DataType dataType;
    /** 张量的形状。 */
    int[] dimensions;
    /** 张量的数据排列格式，排列格式的定义请查看{@link Format}。 */
    enum Format format;
    /** 张量的数据，数据保存在共享内存中，共享内存的定义请查看{@link SharedBuffer}。 */
    struct SharedBuffer data;
};

/**
 * @brief 量化类型。需要配合{@link Node}使用。
 *
 * @since 3.2
 * @version 1.0
 */
enum QuantType: byte {
    /** 不使用量化 */
    QUANT_TYPE_NONE,
    /** int8全量化*/
    QUANT_TYPE_ALL,
};

/**
 * @brief 算子类型。
 *
 * @since 3.2
 * @version 1.0
 */
enum NodeType : unsigned int {
    /** 算子类型为NONE */
    NODE_TYPE_NONE = 0,
    /** 激活函数类型 */
    NODE_TYPE_ACTIVATION = 2,
    /** ADD算子 */
    NODE_TYPE_ADD_FUSION = 5,
    /** ArgMax算子 */
    NODE_TYPE_ARGMAX_FUSION = 11,
    /** AVGPOOL算子 */
    NODE_TYPE_AVGPOOL_FUSION = 17,
    /** BatchToSpaceND算子 */
    NODE_TYPE_BATCH_TO_SPACE_ND = 22,
    /** BiasAdd算子 */
    NODE_TYPE_BIAS_ADD = 23,
    /** Cast算子 */
    NODE_TYPE_CAST = 28,
    /** Concat算子 */
    NODE_TYPE_CONCAT = 31,
    /** Conv2D算子，包含了普通卷积、可分离卷积和分组卷积 */
    NODE_TYPE_CONV2D_FUSION = 35,
    /** 二维反卷积算子 */
    NODE_TYPE_CONV2D_TRANSPOSE_FUSION = 36,
    /** Div算子 */
    NODE_TYPE_DIV_FUSION = 47,
    /** 元素级别算子 */
    NODE_TYPE_ELTWISE = 52,
    /** ExpandDims张算子 */
    NODE_TYPE_EXPAND_DIMS = 56,
    /** Fill算子 */
    NODE_TYPE_FILL = 66,
    /** FullConnection算子 */
    NODE_TYPE_FULL_CONNECTION = 67,
    /** BatchNorm算子 */
    NODE_TYPE_FUSED_BATCH_NORM = 68,
    /** Gather算子 */
    NODE_TYPE_GATHER = 69,
    /** LayerNorm算子 */
    NODE_TYPE_LAYER_NORM_FUSION = 75,
    /** LessEqual算子 */
    NODE_TYPE_LESS_EQUAL = 78,
    /** MatMul算子 */
    NODE_TYPE_MATMUL_FUSION = 89,
    /** Maximum算子 */
    NODE_TYPE_MAXIMUM = 90,
    /** MaxPool算子 */
    NODE_TYPE_MAX_POOL_FUSION = 92,
    /** Mul算子 */
    NODE_TYPE_MUL_FUSION = 99,
    /** OneHot算子 */
    NODE_TYPE_ONE_HOT = 105,
    /** Pad算子 */
    NODE_TYPE_PAD_FUSION = 107,
    /** Pow算子 */
    NODE_TYPE_POW_FUSION = 110,
    /** PReLU算子 */
    NODE_TYPE_PRELU_FUSION = 112,
    /** QuantDTypeCast算子 */
    NODE_TYPE_QUANT_DTYPE_CAST = 113,
    /** Reduce算子 */
    NODE_TYPE_REDUCE_FUSION = 118,
    /** Reshape算子 */
    NODE_TYPE_RESHAPE = 119,
    /** Resize算子 */
    NODE_TYPE_RESIZE = 120,
    /** Rsqrt算子 */
    NODE_TYPE_RSQRT = 126,
    /** Scale算子 */
    NODE_TYPE_SCALE_FUSION = 127,
    /** Shape算子 */
    NODE_TYPE_SHAPE = 130,
    /** Slice算子 */
    NODE_TYPE_SLICE_FUSION = 135,
    /** Softmax算子 */
    NODE_TYPE_SOFTMAX = 138,
    /** SpaceToBatchND算子 */
    NODE_TYPE_SPACE_TO_BATCH_ND = 141,
    /** Split算子 */
    NODE_TYPE_SPLIT = 145,
    /** Sqrt算子 */
    NODE_TYPE_SQRT = 146,
    /** SquaredDifference算子 */
    NODE_TYPE_SQUEEZE = 147,
    /** Squeeze算子 */
    NODE_TYPE_SQUARED_DIFFERENCE = 149,
    /** Stack算子 */
    NODE_TYPE_STACK = 150,
    /** StridedSlice算子 */
    NODE_TYPE_STRIDED_SLICE = 151,
    /** Sub算子 */
    NODE_TYPE_SUB_FUSION = 152,
    /** Tile算子 */
    NODE_TYPE_TILE_FUSION = 160,
    /** TopK算子 */
    NODE_TYPE_TOPK_FUSION = 161,
    /** Transpose算子 */
    NODE_TYPE_TRANSPOSE = 162,
    /** Unsqueeze算子 */
    NODE_TYPE_UNSQUEEZE = 165,
};

/**
 * @brief 调整尺寸的方法。需要配合{@link Resize}算子使用。
 *
 * @since 3.2
 * @version 1.0
 */
enum ResizeMethod : byte {
    /** 未知，默认值。 */
    RESIZE_METHOD_UNKNOWN = -1,
    /** 双线性插值。
     *
     * 假设需要计算未知函数f在点\f$ (x,y) \f$的值其中\f$ x_1<x<x_2,y_1<y<y_2 \f$，
     * 并且已知四个坐标点的值 \f$ Q_{11} = (x_1, y_1), Q_{12} = (x1, y2), Q_{21} = (x_2, y_1)，Q_{22} = (x_2, y_2) \f$，
     * 并且\f$f(Q_{11}),f(Q_{12}),f(Q_{21}),f(Q_{22}) \f$表示四个点的数值，则通过如下公式可计算 \f$ f(x,y) \f$的值：
      \f[
         f(x,y_1) = \frac{x_2-x}{x_2-x_1}f(Q_{11})+\frac{x-x_1}{x_2-x_1}f(Q_{21})
      \f]

      \f[
         f(x,y_2) = \frac{x_2-x}{x_2-x_1}f(Q_{12})+\frac{x-x_1}{x_2-x_1}f(Q_{22})
      \f]

      \f[
         f(x,y) = \frac{y_2-y}{y_2-y_1}f(x,y_1)+\frac{y-y_1}{y_2-y_1}f(x,y_2)
      \f]
     */
    RESIZE_METHOD_LINEAR = 0,
    /** 最近临近插值。
     *
     * 假设需要计算未知函数f在点\f$ (x,y) \f$的值其中\f$ x_1<x<x_2,y_1<y<y_2 \f$，
     * 并且已知四个坐标点的值 \f$ Q_{11} = (x_1, y_1), Q_{12} = (x1, y2), Q_{21} = (x_2, y_1)，Q_{22} = (x_2, y_2) \f$，
     * 则从4个点中选择距离点\f$ (x,y) \f$最近的点的数值作为 \f$ f(x,y) \f$的值。
     */
    RESIZE_METHOD_NEAREST = 1,
    /** 双三次插值。
     *
     * 双三次插值是取采样点周围16个点的值的加权平均来计算采样点的数值。该参数需要配合{@link Resize}的cubicCoeff和coordinateTransformMode参数使用。
     * 当coordinateTransformMode==COORDINATE_TRANSFORM_MODE_HALF_PIXEL时,cubicCoeff=-0.5，其他情况cubicCoeff=-0.75。插值函数的权重函数如下：
      \f[
         W(x) =
         \begin{cases}
            (cubicCoeff+2)|x|^3 - (cubicCoeff+3)|x|^2 +1 , &\text{if } |x| \leq 1; \cr
            cubicCoeff|x|^3 - 5cubicCoeff|x|^2 + 8cubicCoeff|x| - 4a, &\text{if } 1 \lt |x| \leq 2; \cr
            0, &\text{otherwise.}
        \end{cases}
      \f]
     */
    RESIZE_METHOD_CUBIC = 2
};

/**
 * @brief 坐标变换模式，仅{@link Resize}算子使用这些枚举。
 *
 * 以变换 Width 为例，
 * 记 new_i 为resize之后的Tenosr沿x轴的第i个坐标；
 * 记 old_i 为输入Tensor沿x的轴的对应坐标；
 * 记 newWidth 是resize之后的Tensor沿着x的轴的长度；
 * 记 oldWidth 是输入tensor沿x的轴的长度。
 * 可以通过下面的公式计算出 old_i :
 *
 * COORDINATE_TRANSFORM_MODE_ASYMMETRIC：\f$ old_i = newWidth != 0 ? new_i * oldWidth / newWidth : 0 \f$ <br>
 * COORDINATE_TRANSFORM_MODE_ALIGN_CORNERS：\f$ old_i = newWidth != 1 ? new_i * (oldWidth - 1) / (newWidth - 1) \f$<br>
 * COORDINATE_TRANSFORM_MODE_HALF_PIXEL：\f$ old_i = newWidth > 1 ? (new_x + 0.5) * oldWidth / newWidth - 0.5 : 0 \f$<br>
 *
 * @since 3.2
 * @version 1.0
 */
enum CoordinateTransformMode : byte {
    /** 不进行对齐，直接按照比例缩放 */
    COORDINATE_TRANSFORM_MODE_ASYMMETRIC = 0,
    /** 对齐图像的4个角 */
    COORDINATE_TRANSFORM_MODE_ALIGN_CORNERS = 1,
    /** 对齐像素点中心 */
    COORDINATE_TRANSFORM_MODE_HALF_PIXEL = 2
};

/**
 * @brief 临近算法类型。需要配合{@link Resize}算子使用。
 *
 * @since 3.2
 * @version 1.0
 */
enum NearestMode : byte {
    /** 四舍五入取整 */
    NEAREST_MODE_NORMAL = 0,
    /** 向负无穷取整，例如23.5取整为23，−23.5取整为−24 */
    NEAREST_MODE_ROUND_HALF_DOWN = 1,
    /** 向正无穷取整，例如23.5取整为24，−23.5取整为−23 */
    NEAREST_MODE_ROUND_HALF_UP = 2,
    /** 向下取临近的整数，例如23.5取整为23，−23.5取整为−24 */
    NEAREST_MODE_FLOOR = 3,
    /** 向上取临近的整数整，例如23.5取整为24，−23.5取整为−23 */
    NEAREST_MODE_CEIL = 4
};

/**
 * @brief 激活函数类型。
 *
 * 激活函数使得神经网络模型具有区分非线性函数的能力，这也让神经网络模型可以被应用于众多非线性模型中。
 * {@link NodeAttrTypes.idl}文件中拥有ActivationType类型的参数的算子会在运行完成算子的运算之后执行相对应的激活函数。
 *
 * @since 3.2
 * @version 1.0
 */
enum ActivationType : byte {
    /** 无激活函数。 */
    ACTIVATION_TYPE_NO_ACTIVATION = 0,
    /**
     * ReLU激活函数。
     *
     * 逐元素求 \f$ max(x_i, 0) \f$ ，负数输出值会被修改为0，正数输出不受影响。
     \f[
        \text{ReLU}(x_i) = (x_i)^+ = \max(x_i, 0),
     \f]
     * 其中 \f$ x_i \f$ 是输入元素。
     */
    ACTIVATION_TYPE_RELU = 1,
    /**
     * Sigmoid激活函数。
     *
     * 按元素计算Sigmoid激活函数。
     * Sigmoid函数定义为：
     \f[
        \text{Sigmoid}(x_i) = \frac{1}{1 + \exp(-x_i)}
     \f]
     * 其中 \f$ x_i \f$ 是输入的元素。
     */
    ACTIVATION_TYPE_SIGMOID = 2,
    /**
     * ReLU6激活函数。
     *
     * ReLU6类似于ReLU，不同之处在于设置了上限，其上限为6，如果输入大于6，输出会被限制为6。
     * ReLU6函数定义为：
     \f[
        \text{ReLU6}(x_i) = \min(\max(0, x_i), 6)
     \f]
     * 其中 \f$ x_i \f$ 是输入元素。
     */
    ACTIVATION_TYPE_RELU6 = 3,
    /**
     * 指数线性单元激活函数（Exponential Linear Unit activation function，ELU）激活函数。
     *
     * 对输入的每个元素计算ELU。
     * ELU函数定义为：
     \f[
        ELU(x_{i}) =
        \begin{cases}
        x_i, &\text{if } x_i \geq 0; \cr
        \alpha * (\exp(x_i) - 1), &\text{otherwise.}
        \end{cases}
     \f]
     * 其中，\f$ x_i \f$ 表示输入的元素，\f$ \alpha \f$ 表示 alpha 参数,该参数通过{@link Activation}进行设置。
     */
    ACTIVATION_TYPE_ELU = 4,
    /**
     * LeakyReLU激活函数。
     *
     * LeakyReLU函数定义为：
     \f[
        \text{LeakyReLU}(x_i) =
        \begin{cases}
            x_i, &\text{if } x_i \geq 0; \cr
            {\alpha} * x_i, &\text{otherwise.}
        \end{cases}
     \f]
     * 其中，\f$ x_i \f$ 表示输入的元素，\f$ \alpha \f$ 表示 alpha 参数,该参数通过{@link Activation}进行设置。
     */
    ACTIVATION_TYPE_LEAKY_RELU = 5,
    /**
     * 计算绝对值的激活函数。
     *
     * 函数定义为：
     \f[
        \text{abs}(x_i) = |x_i|
     \f]
     *  其中，\f$ x_i \f$ 表示输入的元素。
     */
    ACTIVATION_TYPE_ABS = 6,
    /**
     * ReLU1激活函数。
     *
     * ReLU1函数定义为：
     \f[
        \text{ReLU1}(x_i)= \min(\max(0, x_i), 1)
     \f]
     * 其中 \f$ x_i \f$ 是输入元素。
     */
    ACTIVATION_TYPE_RELU1 = 7,
    /**
     * SoftSign激活函数。
     *
     * SoftSign函数定义如下：
     \f[
        \text{SoftSign}(x_i) = \frac{x_i}{1 + |x_i|}
     \f]
     * 其中 \f$ x_i \f$ 是输入。
     */
    ACTIVATION_TYPE_SOFTSIGN = 8,
    /**
     * Softplus激活函数。
     *
     * Softplus为ReLU函数的平滑近似。可对一组数值使用来确保转换后输出结果均为正值。
     * Softplus函数定义如下：
     \f[
        \text{Softplus}(x_i) = \log(1 + \exp(x_i))
     \f]
     * 其中 \f$ x_i \f$ 是输入元素。
     */
    ACTIVATION_TYPE_SOFTPLUS = 9,
    /**
     * Tanh激活函数。
     *
     * Tanh函数定义如下：
     \f[
        tanh(x) = \frac{\exp(x_i) - \exp(-x_i)}{\exp(x_i) + \exp(-x_i)} = \frac{\exp(2x_i) - 1}{\exp(2x_i) + 1}
     \f]
     * 其中\f$ x_i \f$ 表示输入的元素。
     */
    ACTIVATION_TYPE_TANH = 10,
    /**
     * SELU（Scaled exponential Linear Unit）激活函数。
     *
     * SELU函数定义如下：
     \f[
        SELU(x_{i}) =
        scale *
        \begin{cases}
        x_{i}, &\text{if } x_{i} \geq 0; \cr
        \text{alpha} * (\exp(x_i) - 1), &\text{otherwise.}
        \end{cases}
     \f]
     * 其中， \f$ x_i \f$ 是输入元素，\f$ \alpha \f$ 和 \f$ scale \f$ 是预定义的常量（ \f$ \alpha=1.67326324 \f$，\f$ scale=1.05070098 \f$ ）。
     */
    ACTIVATION_TYPE_SELU = 11,
    /**
     * Hard Swish激活函数。
     *
     * Hard Swish函数定义如下：
     *
     \f[
        \text{Hardswish}(x_{i}) = x_{i} * \frac{ReLU6(x_{i} + 3)}{6}
     \f]
     * 其中，\f$ x_i \f$ 表示输入的元素。
     */
    ACTIVATION_TYPE_HSWISH = 12,
    /**
     * Hard Sigmoid激活函数。
     *
     * Hard Sigmoid函数定义如下：
     \f[
        \text{Hardsigmoid}(x_{i}) = max(0, min(1, \frac{x_{i} + 3}{6}))
     \f]
     * 其中，\f$ x_i \f$ 表示输入的元素。
     */
    ACTIVATION_TYPE_HSIGMOID = 13,
    /**
     * ThresholdedReLU激活函数。
     *
     * 类似ReLU激活函数，min数定义如下：
     \f[
        \text{ThresholdedReLU}(x_i) = \min(\max(0, x_i), t)
     \f]
     * 其中\f$ x_i \f$ 是输入元素,\f$ t \f$ 是最大值。
     */
    ACTIVATION_TYPE_THRESHOLDRELU = 14,
    /**
     * Linear激活函数。
     *
     * Linear函数定义如下：
     \f[
        \text{Linear}(x_i) = x_i
     \f]
     * 其中 \f$ x_i \f$ 是输入元素。
     */
    ACTIVATION_TYPE_LINEAR = 15,
    /**
     * HardTanh激活函数。
     *
     * HardTanh函数定义如下：
     \f[
       \text{HardTanh}(x_i) =
       \begin{cases}
            \text{max_val} & \text{ if } x_i > \text{ max_val } \\
            \text{min_val} & \text{ if } x_i < \text{ min_val } \\
            x_i & \text{ otherwise } \\
        \end{cases}
     \f]
     * 其中\f$ x_i \f$ 是输入,\f$ max\_val \f$ 是最大值，\f$ min\_val \f$ 是最小值，这两个参数通过{@link Activation}进行设置。
     */
    ACTIVATION_TYPE_HARD_TANH = 16,
    /**
     * Sign激活函数。
     *
     * Sign函数定义如下：
     \f[
        Sign(x_i) = \begin{cases} -1, &if\ x_i < 0 \cr
        0, &if\ x_i = 0 \cr
        1, &if\ x_i > 0\end{cases}
     \f]
     * 其中\f$ x_i \f$ 是输入。
     */
    ACTIVATION_TYPE_SIGN = 17,
    /**
     * Swish激活函数。
     *
     * Swish激活函数定义如下：
     \f[
        \text{Swish}(x_i) = x_i * Sigmoid(x_i)
     \f]
     * 其中\f$ x_i \f$ 是输入。
     */
    ACTIVATION_TYPE_SWISH = 18,
    /**
     * GELU（Gaussian error linear unit activation function）高斯误差线性单元激活函数。
     *
     * GELU函数定义如下：
     \f[
        GELU(x_i) = x_i*P(X < x_i)
     \f]
     * 其中\f$ x_i \f$是输入元素，其中\f$ P \f$是标准高斯分布的累积分布函数。
     * 需要通过{@link Activation}的approximate参数指定是否使用近似。
     */
    ACTIVATION_TYPE_GELU = 19,
    /** 未知 */
    ACTIVATION_TYPE_UNKNOWN = 20
};

/**
 * @brief 用于维度移除的方法，需要配合{@link ReduceFusion}算子使用。
 *
 * @since 3.2
 * @version 1.0
 */
enum ReduceMode : byte {
    /** 使用指定维度所有元素的平均值代替该维度的其他元素，以移除该维度。 */
    REDUCE_MODE_MEAN = 0,
    /** 使用指定维度所有元素的最大值代替该维度的其他元素，以移除该维度。 */
    REDUCE_MODE_MAX = 1,
    /** 使用指定维度所有元素的最小值代替该维度的其他元素，以移除该维度。 */
    REDUCE_MODE_MIN = 2,
    /** 使用指定维度所有元素的乘积代替该维度的其他元素，以移除该维度。 */
    REDUCE_MODE_PROD = 3,
    /** 使用指定维度所有元素的加和代替该维度的其他元素，以移除该维度。 */
    REDUCE_MODE_SUM = 4,
    /** 使用指定维度所有元素的平方和替该维度的其他元素，以移除该维度。 */
    REDUCE_MODE_SUM_SQUARE = 5,
    /** 使用指定维度所有元素的绝对值和代替该维度的其他元素，以移除该维度。 */
    REDUCE_MODE_ASUM = 6,
    /** 使用指定维度所有元素的逻辑与代替该维度的其他元素，以移除该维度。 */
    REDUCE_MODE_ALL = 7
};

/**
 * @brief 元素级别运算支持的计算类型，需要配合{@link Eltwise}算子使用。
 *
 * @since 3.2
 * @version 1.0
 */
enum EltwiseMode : byte {
    /** 两个tensor对应元素乘积。 */
    ELTWISE_MODE_PROD = 0,
    /** 两个tensor对应元素之差。 */
    ELTWISE_MODE_SUM = 1,
    /** 两个tensor对应元素的最大值。 */
    ELTWISE_MODE_MAXIMUM = 2,
    /** 未知。 */
    ELTWISE_MODE_UNKNOWN = 3
};

/**
 * @brief 填充类型，需要配合{@link AvgPoolFusion}，{@link AvgPoolFusion}，{@link Conv2DFusion}，{@link MaxPoolFusion}使用。
 *
 * @since 3.2
 * @version 1.0
 */
enum PadMode : byte {
    /**
     * 在输入的高度和宽度方向上填充0。
     * 若设置该模式，算子的padding参数必须大于等于0。
     */
    PAD_MODE_PAD = 0,
    /**
     * 输出的高度和宽度分别与输入整除 stride 后的值相同。
     * 若设置该模式，算子的padding参数必须为0。
     */
    PAD_MODE_SAME = 1,
    /**
     * 在不填充的前提下返回有效计算所得的输出。不满足计算的多余像素会被丢弃。
     * 若设置此模式，则算子的padding参数必须为0。
     */
    PAD_MODE_VALID = 2,
};

/**
 * @brief 小数取整算法，需要配合{@link AvgPoolFusion}算子使用。
 *
 * @since 3.2
 * @version 1.0
 */
enum RoundMode : byte {
    /** 向下取临近的整数，例如23.5取整为23，−23.5取整为−24 */
    ROUND_MODE_FLOOR = 0,
    /** 向上取临近的整数整，例如23.5取整为24，−23.5取整为−23 */
    ROUND_MODE_CEIL = 1
};

/**
 * @brief 填充类型，需要配合{@link PadFusion}算子使用。
 *
 * 当输入的张量x=\f$[[1,2,3],[4,5,6],[7,8,9]]\f$，paddings=\f$[[2,2],[2,2]] \f$ 时效果如下：<br>
 *  paddingMode==PADDING_MODE_CONSTANT并且constantValue = 0时输出为：
 *
      \f$[[0. 0. 0. 0. 0. 0. 0.],\\
          [0. 0. 0. 0. 0. 0. 0.],\\
          [0. 0. 1. 2. 3. 0. 0.],\\
          [0. 0. 4. 5. 6. 0. 0.],\\
          [0. 0. 7. 8. 9. 0. 0.],\\
          [0. 0. 0. 0. 0. 0. 0.],\\
          [0. 0. 0. 0. 0. 0. 0.]]\\ \f$
 *
 *  paddingMode==PADDING_MODE_REFLECT输出为：
 *
      \f$[[9. 8. 7. 8. 9. 8. 7.],\\
          [6. 5. 4. 5. 6. 5. 4.],\\
          [3. 2. 1. 2. 3. 2. 1.],\\
          [6. 5. 4. 5. 6. 5. 4.],\\
          [9. 8. 7. 8. 9. 8. 7.],\\
          [6. 5. 4. 5. 6. 5. 4.],\\
          [3. 2. 1. 2. 3. 2. 1.]]\\ \f$
 *
 *  paddingMode==PADDING_MODE_SYMMETRIC输出为：
 *
      \f$[[5. 4. 4. 5. 6. 6. 5.],\\
          [2. 1. 1. 2. 3. 3. 2.],\\
          [2. 1. 1. 2. 3. 3. 2.],\\
          [5. 4. 4. 5. 6. 6. 5.],\\
          [8. 7. 7. 8. 9. 9. 8.],\\
          [8. 7. 7. 8. 9. 9. 8.],\\
          [5. 4. 4. 5. 6. 6. 5.]]\\ \f$
 *
 *
 * @since 3.2
 * @version 1.0
 */

enum PaddingMode : byte {
    /** 使用常量填充，默认值为0。 */
    PADDING_MODE_CONSTANT = 0,
    /** 以数据区的便捷为轴，使填充区和数据区的数据以该轴保持对称。 */
    PADDING_MODE_REFLECT = 1,
    /** 此填充方法类似于 {@link PADDING_MODE_REFLECT}，它以待填充区和数据区的交界为轴，使待填充区和数据区的数据以该轴保持对称。 */
    PADDING_MODE_SYMMETRIC = 2,
    /** 预留，暂未使用。 */
    PADDING_MODE_RESERVED = 3
};

/** @} */