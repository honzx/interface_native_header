/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Ril
 * @{
 *
 * @brief Ril模块接口定义。
 *
 * Ril模块为上层电话服务提供相关调用接口，涉及电话、短信、彩信、网络搜索、SIM卡等功能接口及各种回调等。
 *
 * @since 3.2
 * @version 1.1
 */

/**
 * @file IRil.idl
 *
 * @brief Ril模块的请求接口。
 *
 * 模块包路径：ohos.hdi.ril.v1_1
 *
 * 引用：
 * - ohos.hdi.ril.v1_1.IRilCallback
 * - ohos.hdi.ril.v1_1.Types
 *
 * @since 3.2
 * @version 1.1
 */


package ohos.hdi.ril.v1_1;
import ohos.hdi.ril.v1_1.IRilCallback;
import ohos.hdi.ril.v1_1.Types;

/**
 * @brief Ril模块的请求接口。
 *
 * 请求接口包括打电话、发短信彩信、激活SIM卡、上网等。
 *
 * @since 3.2
 * @version 1.1
 */
interface IRil {
     /**
      * @brief 设置IRil回调接口，回调函数参考{@link IRilCallback}。
      *
      * @param rilCallback 要设置的回调函数。
      *
      * @return 0 表示执行成功。
      * @return 非零值 表示操作失败。
      *
      * @since 3.2
      * @version 1.0
      */
    [oneway] SetCallback([in] IRilCallback rilCallback);

    /**
     * @brief 设置紧急呼叫号码。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param emergencyInfoList 表示紧急号码列表，详见{@link EmergencyInfoList}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SetEmergencyCallList([in] int slotId, [in] int serialId, [in] struct EmergencyInfoList emergencyInfoList);

    /**
     * @brief 获取紧急号码。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetEmergencyCallList([in] int slotId, [in] int serialId);

    /**
     * @brief 获取通话状态列表。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetCallList([in] int slotId, [in] int serialId);

    /**
     * @brief 拨打电话。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param dialInfo 表示拨号信息，详见{@link DialInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] Dial([in] int slotId, [in] int serialId, [in] struct DialInfo dialInfo);

    /**
     * @brief 拒接电话。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] Reject([in] int slotId, [in] int serialId);

    /**
     * @brief 挂断电话。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param callId 表示通话ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] Hangup([in] int slotId, [in] int serialId, [in] int callId);

    /**
     * @brief 接听电话。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] Answer([in] int slotId, [in] int serialId);

    /**
     * @brief 保持通话。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] HoldCall([in] int slotId, [in] int serialId);

    /**
     * @brief 取消保持通话。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] UnHoldCall([in] int slotId, [in] int serialId);

    /**
     * @brief 切换通话。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SwitchCall([in] int slotId, [in] int serialId);

    /**
     * @brief 合并为会议电话。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param callType 表示通话类型，当前只能为0（即语音通话）。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] CombineConference([in] int slotId, [in] int serialId, [in] int callType);

    /**
     * @brief 与会议电话分离。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param callId 表示通话ID。
     * @param callType 表示通话类型，当前只能为0（即语音通话）。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SeparateConference([in] int slotId, [in] int serialId, [in] int callId, [in] int callType);

    /**
     * @brief 获取呼叫等待。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetCallWaiting([in] int slotId, [in] int serialId);

    /**
     * @brief 设置呼叫等待。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param activate 表示禁止或使能呼叫等待功能，0表示禁止，1表示使能。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SetCallWaiting([in] int slotId, [in] int serialId, [in] int activate);

    /**
     * @brief 获取呼叫转移。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param reason 表示呼叫转移的类型，0表示无条件转移，1表示用户忙时转移，2表示无回复时转移，3表示无法接通时转移。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetCallTransferInfo([in] int slotId, [in] int serialId, [in] int reason);

    /**
     * @brief 设置呼叫转移。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param callForwardSetInfo 表示呼叫转移信息，详见{@link CallForwardSetInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SetCallTransferInfo([in] int slotId, [in] int serialId,
        [in] struct CallForwardSetInfo callForwardSetInfo);

    /**
     * @brief 获取呼叫限制。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param fac 表示呼叫限制操作对象。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetCallRestriction([in] int slotId, [in] int serialId, [in] String fac);

    /**
     * @brief 设置呼叫限制。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param callRestrictionInfo 表示呼叫限制信息，详见{@link CallRestrictionInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SetCallRestriction([in] int slotId, [in] int serialId,
        [in] struct CallRestrictionInfo callRestrictionInfo);

    /**
     * @brief 获取主叫号码显示(CLIP)。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetClip([in] int slotId, [in] int serialId);

    /**
     * @brief 设置主叫号码显示。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param action 表示禁止或使能主叫号码显示功能，0表示禁止，1表示使能。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SetClip([in] int slotId, [in] int serialId, [in] int action);

    /**
     * @brief 获取主叫号码显示限制(CLIR)。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetClir([in] int slotId, [in] int serialId);

    /**
     * @brief 设置主叫号码显示限制。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param action 表示禁止或使能主叫号码显示限制功能，0表示禁止，1表示使能。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SetClir([in] int slotId, [in] int serialId, [in] int action);

    /**
     * @brief 设置通话偏好模式。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param mode 表示通话偏好模式，1表示仅电路（CS)域通话，2表示电路（CS)域通话优先，3表示IP多媒体系统（IMS）通话优先，4表示仅IP多媒体系统（IMS）通话。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SetCallPreferenceMode([in] int slotId, [in] int serialId, [in] int mode);

    /**
     * @brief 获取通话偏好模式。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetCallPreferenceMode([in] int slotId, [in] int serialId);

    /**
     * @brief 设置非结构化补充数据业务（USSD）。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param str 表示USSD信息，最大长度为160个字符。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SetUssd([in] int slotId, [in] int serialId, [in] String str);

    /**
     * @brief 获取Ussd业务。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetUssd([in] int slotId, [in] int serialId);

    /**
     * @brief 设置静音。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param mute 表示禁止或使能静音，0表示禁止，1表示使能。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SetMute([in] int slotId, [in] int serialId, [in] int mute);

    /**
     * @brief 获取静音。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetMute([in] int slotId, [in] int serialId);

    /**
     * @brief 获取通话失败原因。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetCallFailReason([in] int slotId, [in] int serialId);

    /**
     * @brief 通话保持和恢复。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param type 表示挂断的通话类型，0表示直接挂断，1表示挂断前台和后台，2表示挂断前台、恢复后台，3表示挂断所有通话。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] CallSupplement([in] int slotId, [in] int serialId, [in] int type);

    /**
     * @brief 发送双音多频（DTMF）。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param dtmfInfo 表示DTMF信息，详见{@link DtmfInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SendDtmf([in] int slotId, [in] int serialId, [in] struct DtmfInfo dtmfInfo);

    /**
     * @brief 开启DTMF。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param dtmfInfo 表示DTMF信息，详见{@link DtmfInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] StartDtmf([in] int slotId, [in] int serialId, [in] struct DtmfInfo dtmfInfo);

    /**
     * @brief 关闭DTMF。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param dtmfInfo 表示DTMF信息，详见{@link DtmfInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] StopDtmf([in] int slotId, [in] int serialId, [in] struct DtmfInfo dtmfInfo);

    /**
     * @brief 设置呼叫限制密码。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param setBarringInfo 表示设置呼叫限制密码的信息，详见{@link SetBarringInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SetBarringPassword([in] int slotId, [in] int serialId, [in] struct SetBarringInfo setBarringInfo);

    /**
     * @brief 设置Vonr开关。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param status 表示Vonr开关状态，0表示OFF，1表示ON。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 4.0
     * @version 1.1
     */
    [oneway] SetVonrSwitch([in] int slotId, [in] int serialId, [in] int status);

    /**
     * @brief 激活数据业务。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param dataCallInfo 表示数据业务信息，详见{@link DataCallInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] ActivatePdpContext([in] int slotId, [in] int serialId, [in] struct DataCallInfo dataCallInfo);

    /**
     * @brief 断开数据业务。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param uniInfo 表示通用信息，详见{@link UniInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] DeactivatePdpContext([in] int slotId, [in] int serialId, [in] struct UniInfo uniInfo);

    /**
     * @brief 获取当前所有数据连接状态。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param uniInfo 表示通用信息，详见{@link UniInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetPdpContextList([in] int slotId, [in] int serialId, [in] struct UniInfo uniInfo);

    /**
     * @brief 设置初始化默认网络接入技术(APN)信息。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param dataProfileDataInfo 表示分组报文协议(PDP)上下文信息，详见{@link DataProfileDataInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SetInitApnInfo([in] int slotId, [in] int serialId, [in] struct DataProfileDataInfo dataProfileDataInfo);

    /**
     * @brief 获取当前链路信息。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param cid PDP上下文标识符。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetLinkBandwidthInfo([in] int slotId, [in] int serialId, [in] int cid);

    /**
     * @brief 获取链接功能。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 4.0
     * @version 1.1
     */
    [oneway] GetLinkCapability([in] int slotId, [in] int serialId);

    /**
     * @brief 设置当前链路信息的上报规则。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param dataLinkBandwidthReportingRule 表示网络频率上报规则，详见{@link DataLinkBandwidthReportingRule}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SetLinkBandwidthReportingRule([in] int slotId, [in] int serialId,
        [in] struct DataLinkBandwidthReportingRule dataLinkBandwidthReportingRule);

    /**
     * @brief 使能SIM卡槽数据业务。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param dataPermitted 表示是否使能，0表示不使能，1表示使能。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SetDataPermitted([in] int slotId, [in] int serialId, [in] int dataPermitted);

    /**
     * @brief 设置数据业务使用的PDP上下文信息。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param dataProfilesInfo 表示PDP上下文信息列表，详见{@link DataProfilesInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SetDataProfileInfo([in] int slotId, [in] int serialId, [in] struct DataProfilesInfo dataProfilesInfo);

    /**
     * @brief 发送数据业务性能模式。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param dataPerformanceInfo 表示数据业务性能模式，详见{@link DataPerformanceInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SendDataPerformanceMode([in] int slotId, [in] int serialId, [in] struct DataPerformanceInfo dataPerformanceInfo);

    /**
     * @brief 发送数据业务睡眠模式。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param dataSleepInfo 表示数据业务睡眠模式，详见{@link DataSleepInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SendDataSleepMode([in] int slotId, [in] int serialId, [in] struct DataSleepInfo dataSleepInfo);

    /**
     * @brief 设置Modem状态。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param fun 表示功能模式，0表示最小模式，1表示online模式，4表示offline模式，其他模式由芯片自定义。
     * @param rst 表示Modem是否自动复位，0表示不复位，1表示复位。
     *
     * @return 0 表示执行成功
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SetRadioState([in] int slotId, [in] int serialId, [in] int fun, [in] int rst);

    /**
     * @brief 获取Modem状态。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetRadioState([in] int slotId, [in] int serialId);

    /**
     * @brief 获取国际移动设备识别码。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetImei([in] int slotId, [in] int serialId);

    /**
     * @brief 获取移动设备识别码。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetMeid([in] int slotId, [in] int serialId);

    /**
     * @brief 获取电路（CS）域接入技术。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetVoiceRadioTechnology([in] int slotId, [in] int serialId);

    /**
     * @brief 获取基带版本。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetBasebandVersion([in] int slotId, [in] int serialId);

    /**
     * @brief 发送手机正在关机状态到Modem。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] ShutDown([in] int slotId, [in] int serialId);

    /**
     * @brief 获取SIM卡数据。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param SimIoRequestInfo 表示SIM卡数据请求信息，详见{@link SimIoRequestInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetSimIO([in] int slotId, [in] int serialId, [in] struct SimIoRequestInfo simIO);

    /**
     * @brief 获取SIM卡状态。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetSimStatus([in] int slotId, [in] int serialId);

    /**
     * @brief 获取SIM卡国际移动用户识别码。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetImsi([in] int slotId, [in] int serialId);

    /**
     * @brief 获取SIM卡锁状态。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param SimLockInfo 表示SIM卡锁信息，详见{@link SimLockInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetSimLockStatus([in] int slotId, [in] int serialId, [in] struct SimLockInfo simLockInfo);

    /**
     * @brief 设置SIM卡锁。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param SimLockInfo 表示SIM卡锁信息，详见{@link SimLockInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SetSimLock([in] int slotId, [in] int serialId, [in] struct SimLockInfo simLockInfo);

    /**
     * @brief 修改SIM卡密码。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param simPassword 表示SIM卡密码信息，详见{@link SimPasswordInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] ChangeSimPassword([in] int slotId, [in] int serialId, [in] struct SimPasswordInfo simPassword);

    /**
     * @brief PIN解锁。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param pin 表示用于解锁的PIN码。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] UnlockPin([in] int slotId, [in] int serialId, [in] String pin);

    /**
     * @brief PUK解锁。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param puk 表示用于解锁的PUK码。
     * @param pin 表示用于解锁的PIN码。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] UnlockPuk([in] int slotId, [in] int serialId, [in] String puk, [in] String pin);

    /**
     * @brief PIN2解锁。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param pin2 表示用于解锁的PIN2码。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] UnlockPin2([in] int slotId, [in] int serialId, [in] String pin2);

    /**
     * @brief PUK2解锁。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param puk2 表示用于解锁的PUK2码。
     * @param pin2 表示用于解锁的PIN2码。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] UnlockPuk2([in] int slotId, [in] int serialId, [in] String puk2, [in] String pin2);

    /**
     * @brief 激活去激活SIM卡。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param index 表示索引值。
     * @param enable 表示激活状态，0为去激活，1为激活。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SetActiveSim([in] int slotId, [in] int serialId, [in] int index, [in] int enable);

    /**
     * @brief 发送SIM卡应用开发工具箱(STK) TerminalResponse指令。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param strCmd 表示指令的字串文本。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SimStkSendTerminalResponse([in] int slotId, [in] int serialId, [in] String strCmd);

    /**
     * @brief 发送STK Envelope指令。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param strCmd 表示指令的字串文本。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SimStkSendEnvelope([in] int slotId, [in] int serialId, [in] String strCmd);

    /**
     * @brief 发送STK CallSetup指令。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param accept 表示是否接受CallSetup请求，0为不接受，1为接受。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SimStkSendCallSetupRequestResult([in] int slotId, [in] int serialId, [in] int accept);

    /**
     * @brief 获取STK是否Ready状态。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SimStkIsReady([in] int slotId, [in] int serialId);

    /**
     * @brief 获取主副卡协议栈。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetRadioProtocol([in] int slotId,[in] int serialId);

    /**
     * @brief 设置主副卡协议栈。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param radioProtocol 表示Radio协议信息，详见{@link RadioProtocol}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SetRadioProtocol([in] int slotId,[in] int serialId,[in] struct RadioProtocol radioProtocol);

    /**
     * @brief 打开应用协议数据单元（APDU）逻辑通道。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param appID 表示应用标识符。
     * @param p2 表示AT指令码的参数2。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SimOpenLogicalChannel([in] int slotId, [in] int serialId, [in] String appID, [in] int p2);

    /**
     * @brief 关闭应用协议数据单元（APDU）逻辑通道。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param channelId 表示请求关闭的逻辑通道ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SimCloseLogicalChannel([in] int slotId, [in] int serialId, [in] int channelId);

    /**
     * @brief 应用协议数据单元（APDU）逻辑通道数据传输，由应用主动发起连接和关闭。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param apduSimIO 表示通过应用协议数据单元（APDU）传输的SIM数据请求信息，详见{@link ApduSimIORequestInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SimTransmitApduLogicalChannel([in] int slotId, [in] int serialId,
        [in] struct ApduSimIORequestInfo apduSimIO);

    /**
     * @brief 应用协议数据单元（APDU）基础通道数据传输，默认打开的传输通道。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param apduSimIO 表示通过应用协议数据单元（APDU）传输的SIM数据请求信息，详见{@link ApduSimIORequestInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SimTransmitApduBasicChannel([in] int slotId, [in] int serialId,
        [in] struct ApduSimIORequestInfo apduSimIO);

    /**
     * @brief SIM卡鉴权。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param simAuthInfo 表示SIM卡鉴权请求信息，详见{@link SimAuthenticationRequestInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SimAuthentication([in] int slotId, [in] int serialId,
        [in] struct SimAuthenticationRequestInfo simAuthInfo);

    /**
     * @brief 解锁SIM卡。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param lockType 表示锁类型，参考3GPP TS 22.022 [33]。
     * @param key 表示用于解锁的密码，参考3GPP TS 22.022 [33]。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] UnlockSimLock([in] int slotId, [in] int serialId, [in] int lockType, [in] String key);

    /**
     * @brief 获取信号强度。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetSignalStrength([in] int slotId, [in] int serialId);

    /**
     * @brief 获取电路(CS)域注册状态。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetCsRegStatus([in] int slotId, [in] int serialId);

    /**
     * @brief 获取分组(PS)域注册状态。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetPsRegStatus([in] int slotId, [in] int serialId);

    /**
     * @brief 获取运营商名称信息。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetOperatorInfo([in] int slotId, [in] int serialId);

    /**
     * @brief 获取可用网络信息。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetNetworkSearchInformation([in] int slotId, [in] int serialId);

    /**
     * @brief 获取选网模式。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetNetworkSelectionMode([in] int slotId, [in] int serialId);

    /**
     * @brief 设置选网模式。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param networkModeInfo 表示选网模式信息，详见{@link SetNetworkModeInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SetNetworkSelectionMode([in] int slotId, [in] int serialId,
        [in] struct SetNetworkModeInfo networkModeInfo);

    /**
     * @brief 获取相邻小区信息。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetNeighboringCellInfoList([in] int slotId, [in] int serialId);

    /**
     * @brief 获取小区信息。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetCurrentCellInfo([in] int slotId, [in] int serialId);

    /**
     * @brief 设置首选网络类型。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param preferredNetworkType 表示首选网络类型，详见{@link PreferredNetworkTypeInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SetPreferredNetwork([in] int slotId, [in] int serialId, [in] int preferredNetworkType);

    /**
     * @brief 获取首选网络类型。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetPreferredNetwork([in] int slotId, [in] int serialId);

    /**
     * @brief 获取物理通道配置。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetPhysicalChannelConfig([in] int slotId, [in] int serialId);

    /**
     * @brief 设置小区位置更新通知模式。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param mode 表示通知模式，详见{@link RilRegNotifyMode}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SetLocateUpdates([in] int slotId, [in] int serialId, [in] enum RilRegNotifyMode mode);

    /**
     * @brief 设置Modem主动上报消息过滤器。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param newFilter 表示消息类型过滤器，使用二进制标志位表示不同的消息类型，0表示关闭，
     * 1表示信号强度，2表示网络注册状态，4表示数据连接状态，8表示链路容量，16表示物理通道配置。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SetNotificationFilter([in] int slotId, [in] int serialId, [in] int newFilter);

    /**
     * @brief 设置设备状态。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param deviceStateType 表示设备状态类型，0表示省电模式，1表示充电模式，2表示低数据模式。
     * @param deviceStateOn 表示设备状态开关，0表示关闭，1表示开启。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SetDeviceState([in] int slotId, [in] int serialId, [in] int deviceStateType, [in] int deviceStateOn);

    /**
     * @brief 发送全球移动通信系统 (GSM)短信。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param gsmSmsMessageInfo 表示GSM短信信息，详见{@link GsmSmsMessageInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SendGsmSms([in] int slotId, [in] int serialId, [in] struct GsmSmsMessageInfo gsmSmsMessageInfo);

    /**
     * @brief 发送码分多址(CDMA)短信。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param cdmaSmsMessageInfo 表示CDMA短信信息，详见{@link SendCdmaSmsMessageInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SendCdmaSms([in] int slotId, [in] int serialId, [in] struct SendCdmaSmsMessageInfo cdmaSmsMessageInfo);

    /**
     * @brief 写入GSM SIM卡短信。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param gsmSmsMessageInfo 表示SIM卡短信信息，详见{@link SmsMessageIOInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] AddSimMessage([in] int slotId, [in] int serialId, [in] struct SmsMessageIOInfo gsmSmsMessageInfo);

    /**
     * @brief 删除GSM SIM卡短信。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param index 表示消息索引。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] DelSimMessage([in] int slotId, [in] int serialId, [in] int index);

    /**
     * @brief 更新GSM SIM卡短信。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param gsmSmsMessageInfo 表示SIM卡短信信息，详见{@link SmsMessageIOInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] UpdateSimMessage([in] int slotId, [in] int serialId, [in] struct SmsMessageIOInfo gsmSmsMessageInfo);

    /**
     * @brief 写入CDMA SIM卡短信。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param cdmaSmsMessageInfo 表示SIM卡短信信息，详见{@link SmsMessageIOInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] AddCdmaSimMessage([in] int slotId, [in] int serialId, [in] struct SmsMessageIOInfo cdmaSmsMessageInfo);

    /**
     * @brief 删除CDMA SIM卡短信。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param index 表示消息索引。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] DelCdmaSimMessage([in] int slotId, [in] int serialId, [in] int index);

    /**
     * @brief 更新CDMA SIM卡短信。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param cdmaSmsMessageInfo 表示SIM卡短信信息，详见{@link SmsMessageIOInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] UpdateCdmaSimMessage([in] int slotId, [in] int serialId, [in] struct SmsMessageIOInfo cdmaSmsMessageInfo);

    /**
     * @brief 设置短信中心地址。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param serviceCenterAddress 表示短信中心地址信息，详见{@link ServiceCenterAddress}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SetSmscAddr([in] int slotId, [in] int serialId, [in] struct ServiceCenterAddress serviceCenterAddress);

    /**
     * @brief 获取短信中心地址。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetSmscAddr([in] int slotId, [in] int serialId);

    /**
     * @brief 激活GSM小区广播。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param cellBroadcastInfo 表示GSM小区广播配置信息，详见{@link CBConfigInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SetCBConfig([in] int slotId, [in] int serialId, [in] struct CBConfigInfo cellBroadcastInfo);

    /**
     * @brief 获取GSM小区广播配置。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetCBConfig([in] int slotId, [in] int serialId);

    /**
     * @brief 激活CDMA小区广播。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param cdmaCBConfigInfoList 表示CDMA小区广播配置信息列表，详见{@link CdmaCBConfigInfoList}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SetCdmaCBConfig([in] int slotId, [in] int serialId, [in] struct CdmaCBConfigInfoList cdmaCBConfigInfoList);

    /**
     * @brief 获取CDMA小区广播配置。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] GetCdmaCBConfig([in] int slotId, [in] int serialId);

    /**
     * @brief 发送GSM长短信。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param gsmSmsMessageInfo 表示GSM短信信息，详见{@link GsmSmsMessageInfo}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SendSmsMoreMode([in] int slotId, [in] int serialId, [in] struct GsmSmsMessageInfo gsmSmsMessageInfo);

    /**
     * @brief 确认接收新短信。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param modeData 表示接收短信处理模式，详见{@link ModeData}。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SendSmsAck([in] int slotId, [in] int serialId, [in] struct ModeData modeData);

    /**
     * @brief 发送应答给无线接口层（RIL）。
     *
     * @return 0 表示执行成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] SendRilAck();

    /**
     * @brief 获取RRC连接状态.
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @return Returns 0 表示执行成功。
     * @return Returns 非零值 表示操作失败。
     *
     * @since 4.0
     * @version 1.1
     */
    [oneway] GetRrcConnectionState([in] int slotId, [in] int serialId);

    /**
     * @brief 设置NR选项模式。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @param mode NR的选择模式。
     * @return Returns 0 表示执行成功。
     * @return Returns 非零值 表示操作失败。
     *
     * @since 4.0
     * @version 1.1
     */
    [oneway] SetNrOptionMode([in] int slotId, [in] int serialId, [in] int mode);

    /**
     * @brief 获取NR选项模式。
     *
     * @param slotId 表示卡槽ID。
     * @param serialId 表示请求的序列化ID。
     * @return Returns 0 表示执行成功。
     * @return Returns a 非零值 表示操作失败。
     *
     * @since 4.0
     * @version 1.1
     */
    [oneway] GetNrOptionMode([in] int slotId, [in] int serialId);
}
/** @} */
