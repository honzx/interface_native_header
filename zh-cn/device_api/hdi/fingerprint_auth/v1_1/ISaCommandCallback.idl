/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdfFingerprintAuth
 * @{
 *
 * @brief 提供指纹认证驱动的API接口。
 *
 * 指纹认证驱动程序为指纹认证服务提供统一的接口，用于访问指纹认证驱动程序。获取指纹认证驱动代理后，服务可以调用相关API获取执行器。
 * 获取指纹认证执行器后，服务可以调用相关API获取执行器信息，获取凭据模板信息、注册指纹特征模板、进行用户指纹认证、删除指纹特征模板等。
 *
 * @since 4.0
 */

/**
 * @file ISaCommandCallback.idl
 *
 * @brief  定义异步API接口回调，可以发送命令给SA。见{@link IExecutor}。
 *
 * 模块包路径：ohos.hdi.fingerprint_auth.v1_1
 *
 * 引用：ohos.hdi.fingerprint_auth.v1_1.FingerprintAuthTypes
 *
 * @since 4.0
 */

package ohos.hdi.fingerprint_auth.v1_1;

import ohos.hdi.fingerprint_auth.v1_1.FingerprintAuthTypes;

/**
 * @brief 定义异步API接口回调，可以发送命令给SA。见{@link IExecutor}。
 *
 * @since 4.0
 * @version 1.1
 */
[callback] interface ISaCommandCallback {
    /**
     * @brief 定义SA命令过程回调函数。
     *
     * @param commands SA命令见{@link SaCommand}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 4.0
     * @version 1.1
     */
    OnSaCommands([in] struct SaCommand[] commands);
}
/** @} */