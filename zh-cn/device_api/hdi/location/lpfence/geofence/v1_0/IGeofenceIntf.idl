/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup HdiLpfenceGeofence
 * @{
 *
 * @brief 为低功耗围栏服务提供地理围栏的API。
 *
 * 本模块接口提供添加圆形和多边形地理围栏，删除地理围栏，获取地理围栏状态信息，获取设备地理位置等功能。本模块可在AP休眠状态下持续工作。
 *
 * 应用场景：判断用户设备是否达到某个精确地理位置区域，从而进行一些后续服务，如门禁卡的切换、定制消息的提醒等。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file IGeofenceIntf.idl
 *
 * @brief 声明基站围栏模块提供的API，用于添加多种地理围栏，删除地理围栏，获取地理围栏状态信息，获取设备地理位置，下发基站离线数据库。
 *
 * 模块包路径：ohos.hdi.location.lpfence.geofence.v1_0
 *
 * 引用：
 * - ohos.hdi.location.lpfence.geofence.v1_0.GeofenceTypes
 * - ohos.hdi.location.lpfence.geofence.v1_0.IGeofenceCallback
 *
 * @since 4.0
 * @version 1.0
 */

package ohos.hdi.location.lpfence.geofence.v1_0;

import ohos.hdi.location.lpfence.geofence.v1_0.GeofenceTypes;
import ohos.hdi.location.lpfence.geofence.v1_0.IGeofenceCallback;

/**
 * @brief 定义对地理围栏模块进行基本操作的接口。
 *
 * 接口包含注册回调函数，取消注册回调函数，添加圆形和多边形地理围栏，删除地理围栏，获取地理围栏状态信息，获取设备地理位置，下发基站离线数据库。
 *
 * @since 4.0
 * @version 1.0
 */
interface IGeofenceInterface {
    /**
     * @brief 注册回调函数。
     *
     * 用户在开启地理围栏功能前，需要先注册该回调函数。当地理围栏状态发生变化时，会通过回调函数进行上报。
     *
     * @param callbackObj 要注册的回调函数，只需成功订阅一次，无需重复订阅。详见{@link IGeofenceCallback}。
     *
     * @return 如果注册回调函数成功，则返回0。
     * @return 如果注册回调函数失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    RegisterGeofenceCallback([in] IGeofenceCallback callbackObj);
    
    /**
     * @brief 取消注册回调函数。
     *
     * 取消之前注册的回调函数。当不需要使用地理围栏功能，或需要更换回调函数时，需要取消注册回调函数。
     *
     * @param callbackObj 要取消注册的回调函数，只需成功取消订阅一次，无需重复取消订阅。详见{@link IGeofenceCallback}。
     *
     * @return 如果取消注册回调函数成功，则返回0。
     * @return 如果取消注册回调函数失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    UnregisterGeofenceCallback([in] IGeofenceCallback callbackObj);
    
    /**
     * @brief 添加圆形地理围栏。
     *
     * 支持一次添加多个圆形地理围栏
     *
     * @param circleGeofences 添加圆形围栏信息。详见{@link GeofenceCircleRequest}。
     *
     * @return 如果添加成功，则返回0。
     * @return 如果添加失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    AddCircleGeofences([in] struct GeofenceCircleRequest[] circleGeofences);
    
    /**
     * @brief 添加多边形地理围栏。
     *
     * 支持一次添加多个多边形地理围栏
     *
     * @param polygonGeofences 添加多边形围栏信息。详见{@link GeofencePolygonRequest}。
     *
     * @return 如果添加成功，则返回0。
     * @return 如果添加失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    AddPolygonGeofences([in] struct GeofencePolygonRequest[] polygonGeofences);
    
    /**
     * @brief 删除地理围栏。
     *
     * 支持一次删除多个地理围栏。
     *
     * @param geofenceId 地理围栏id号。详见{@link GeofenceCircleRequest}和{@link GeofencePolygonRequest}。
     *
     * @return 如果删除成功，则返回0。
     * @return 如果删除失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    RemoveGeofences([in] int[] geofenceId);
    
    /**
     * @brief 获取当前设备与一个地理围栏的状态关系。
     *
     * 设备与地理围栏的状态关系详见{@link GeofenceTransition}定义。
     *
     * @param geofenceId 地理围栏id号。
     *
     * @return 返回位置关系。详见{@link GeofenceTransition}定义。
     * @return 如果调用失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    GetGeofenceStatus([in] int geofenceId);
    
    /**
     * @brief 获取最新的位置信息。
     *
     * 位置信息通过回调函数上报。详见{@link OnGetCurrentLocation}定义。
     *
     * @return 如果调用成功，则返回0。
     * @return 如果调用失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    GetGeofenceLocation();
    
    /**
     * @brief 获取地理围栏使用信息。
     *
     * 查看当前设备支持添加的地理围栏最大个数和已添加的地理围栏个数。通过回调函数上报通知，详见{@link OnGetGeofenceSizeCb}。
     *
     * @return 如果调用成功，则返回0。
     * @return 如果调用失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    GetGeofenceSize();
    
    /**
     * @brief 下发基站离线数据库数据。
     *
     * 若请求离线数据库数据成功，则上层服务通过该接口将数据下发。
     *
     * @param dbData 基站离线数据库数据。详见{@link OfflineDb}定义。
     * @param cellType 基站主区的移动通信技术代。详见{@link GeofenceCellType}定义。
     *
     * @return 如果调用成功，则返回0。
     * @return 如果调用失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    SendCellOfflineDb([in] struct OfflineDb dbData,
                      [in] int cellType);
}
/** @} */