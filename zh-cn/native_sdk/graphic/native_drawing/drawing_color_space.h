/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_COLOR_SPACE_H
#define C_INCLUDE_DRAWING_COLOR_SPACE_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。
 * 本模块不提供像素单位，和应用上下文环境保持一致。如果处于ArkUI开发环境中，采用框架默认像素单位vp。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_color_space.h
 *
 * @brief 文件中定义了与颜色空间相关的功能函数。
 *
 * 引用文件"native_drawing/drawing_color_space.h"
 * @library libnative_drawing.so
 * @since 12
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 创建一个标准颜色空间。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 函数返回一个指针，指针指向创建的颜色空间对象{@link OH_Drawing_ColorSpace}。
 * @since 12
 * @version 1.0
 */
OH_Drawing_ColorSpace* OH_Drawing_ColorSpaceCreateSrgb(void);

/**
 * @brief 创建一个Gamma 1.0空间上的颜色空间。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 函数返回一个指针，指针指向创建的颜色空间对象{@link OH_Drawing_ColorSpace}。
 * @since 12
 * @version 1.0
 */
OH_Drawing_ColorSpace* OH_Drawing_ColorSpaceCreateSrgbLinear(void);

/**
 * @brief 销毁颜色空间对象，并回收该对象占有内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_ColorSpace 指向颜色空间对象{@link OH_Drawing_ColorSpace}的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_ColorSpaceDestroy(OH_Drawing_ColorSpace*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
