/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Sensor
 * @{
 *
 * @brief Provides APIs for sensor services to access the sensor driver.
 *
 * A sensor service can obtain a sensor driver object or agent and then call APIs provided by this object or agent to
 * access different types of sensor devices, thereby obtaining sensor information,
 * subscribing to or unsubscribing from sensor data, enabling or disabling a sensor,
 * setting the sensor data reporting mode, and setting sensor options such as the accuracy and measurement range.
 * 
 * 
 *
 * @version 1.0
 */

/**
 * @file sensor_type.h
 *
 * @brief Defines the data used by the sensor module, including the sensor type, sensor information,
 * and reported sensor data.
 *
 * @since 2.2
 * @version 1.0
 */

#ifndef SENSOR_TYPE_H
#define SENSOR_TYPE_H

#include <stdint.h>

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

/** Maximum length of the sensor name. */
#define SENSOR_NAME_MAX_LEN       32
/** Maximum length of the sensor version. */
#define SENSOR_VERSION_MAX_LEN    16

/**
 * @brief Enumerates the return values of the sensor module.
 *
 * @since 2.2
 */
enum SensorStatus {
    /** The operation is successful. */
    SENSOR_SUCCESS           = 0,
    /** The operation fails. */
    SENSOR_FAILURE           = -1,
    /** The sensor is not supported. */
    SENSOR_NOT_SUPPORT       = -2,
    /** Invalid sensor parameter. */
    SENSOR_INVALID_PARAM     = -3,
    /** Invalid sensor service. */
    SENSOR_INVALID_SERVICE   = -4,
    /** The pointer is null. */
    SENSOR_NULL_PTR          = -5,
};

/**
 * @brief Enumerates the sensor types.
 *
 * @since 2.2
 */
enum SensorTypeTag {
    /** None, for testing only. */
    SENSOR_TYPE_NONE                = 0,
    /** Acceleration sensor. */
    SENSOR_TYPE_ACCELEROMETER       = 1,
    /** Gyroscope sensor. */
    SENSOR_TYPE_GYROSCOPE           = 2,
    /** Photoplethysmography (PPG) sensor. */
    SENSOR_TYPE_PHOTOPLETHYSMOGRAPH = 3,
    /** Electrocardiogram (ECG) sensor. */
    SENSOR_TYPE_ELECTROCARDIOGRAPH  = 4,
    /** Ambient light sensor. */
    SENSOR_TYPE_AMBIENT_LIGHT       = 5,
    /** Magnetic field sensor. */
    SENSOR_TYPE_MAGNETIC_FIELD      = 6,
    /** Capacitive sensor. */
    SENSOR_TYPE_CAPACITIVE          = 7,
    /** Barometer sensor. */
    SENSOR_TYPE_BAROMETER           = 8,
    /** Temperature sensor. */
    SENSOR_TYPE_TEMPERATURE         = 9,
    /** Hall effect sensor. */
    SENSOR_TYPE_HALL                = 10,
    /** Gesture sensor. */
    SENSOR_TYPE_GESTURE             = 11,
    /** Proximity sensor. */
    SENSOR_TYPE_PROXIMITY           = 12,
    /** Humidity sensor. */
    SENSOR_TYPE_HUMIDITY            = 13,
    /** Start ID of medical sensors. */
    SENSOR_TYPE_MEDICAL_BEGIN       = 128,
    /** End ID of medical sensors. */
    SENSOR_TYPE_MEDICAL_END         = 160,
    /** Maximum ID of physical sensors. */
    SENSOR_TYPE_PHYSICAL_MAX        = 255,
    /** Orientation sensor. */
    SENSOR_TYPE_ORIENTATION         = 256,
    /** Gravity sensor. */
    SENSOR_TYPE_GRAVITY             = 257,
    /** Linear acceleration sensor. */
    SENSOR_TYPE_LINEAR_ACCELERATION = 258,
    /** Rotation vector sensor. */
    SENSOR_TYPE_ROTATION_VECTOR     = 259,
    /** Ambient temperature sensor. */
    SENSOR_TYPE_AMBIENT_TEMPERATURE = 260,
    /** Uncalibrated magnetic field sensor. */
    SENSOR_TYPE_MAGNETIC_FIELD_UNCALIBRATED = 261,
    /** Game rotation vector sensor. */
    SENSOR_TYPE_GAME_ROTATION_VECTOR        = 262,
    /** Uncalibrated gyroscope sensor. */
    SENSOR_TYPE_GYROSCOPE_UNCALIBRATED      = 263,
    /** Significant motion sensor. */
    SENSOR_TYPE_SIGNIFICANT_MOTION          = 264,
    /** Pedometer detection sensor. */
    SENSOR_TYPE_PEDOMETER_DETECTION         = 265,
    /** Pedometer sensor. */
    SENSOR_TYPE_PEDOMETER                   = 266,
    /** Geomagnetic rotation vector sensor. */
    SENSOR_TYPE_GEOMAGNETIC_ROTATION_VECTOR = 277,
    /** Heart rate sensor. */
    SENSOR_TYPE_HEART_RATE                  = 278,
    /** Device orientation sensor. */
    SENSOR_TYPE_DEVICE_ORIENTATION          = 279,
    /** Wear detection sensor. */
    SENSOR_TYPE_WEAR_DETECTION              = 280,
    /** Uncalibrated acceleration sensor. */
    SENSOR_TYPE_ACCELEROMETER_UNCALIBRATED  = 281,
    /** Maximum number of sensor types. */
    SENSOR_TYPE_MAX,
};

/**
 * @brief Enumerates the accuracy types of sensors.
 *
 * @since 2.2
 */
enum SensorAccuracyType {
    /** No accuracy. */
    SENSOR_NO_ACCURACY     = 0,
     /** Low accuracy. */
    SENSOR_LOW_ACCURACY    = 1,
    /** Medium accuracy. */
    SENSOR_MEDIUM_ACCURACY = 2,
    /** High accuracy. */
    SENSOR_HIGH_ACCURACY   = 3,
    /** Maximum accuracy. */
    SENSOR_MAX_ACCURACY,
};

/**
 * @brief Enumerates the measurement ranges of sensors.
 *
 * @since 2.2
 */
enum SensorRangeType {
    /** Measurement range level 1. */
    SENSOR_RANGE_LEVEL1 = 0,
    /** Measurement range level 2. */
    SENSOR_RANGE_LEVEL2 = 1,
    /** Measurement range level 3. */
    SENSOR_RANGE_LEVEL3 = 2,
    /** Maximum measurement range. */
    SENSOR_RANGE_LEVEL_MAX,
};

/**
 * @brief Enumerates the data reporting modes of sensors.
 *
 * @since 2.2
 */
enum SensorModeType {
    /** Default data reporting mode. */
    SENSOR_MODE_DEFAULT   = 0,
    /** Reporting a group of data each time in real-time data reporting mode. */
    SENSOR_MODE_REALTIME  = 1,
    /** Reporting data upon status changes in real-time data reporting mode. */
    SENSOR_MODE_ON_CHANGE = 2,
    /** Reporting data only once in real-time data reporting mode. */
    SENSOR_MODE_ONE_SHOT  = 3,
    /** Reporting data based on the configured buffer size in FIFO-based data reporting mode. */
    SENSOR_MODE_FIFO_MODE = 4,
    /** Maximum sensor data reporting mode. */
    SENSOR_MODE_MAX,
};

/**
 * @brief Enumerates the sensor groups.
 *
 * @since 2.2
 */
enum SensorGroupType {
    /** Traditional sensor group, whose value range is not within 128-160. */
    TRADITIONAL_SENSOR_TYPE = 0,
    /** Medical sensor group, whose value range is 128-160. */
    MEDICAL_SENSOR_TYPE = 1,
    /** Maximum sensor group type. */
    SENSOR_GROUP_TYPE_MAX,
};

/**
 * @brief Defines the basic sensor information.
 *
 * The basic sensor information includes the sensor name, vendor, firmware version, hardware version, sensor type ID,
 * sensor ID, maximum measurement range, accuracy, and power.
 *
 * @since 2.2
 */
struct SensorInformation {
    /** Sensor name. */
    char sensorName[SENSOR_NAME_MAX_LEN];
    /** Sensor vendor. */
    char vendorName[SENSOR_NAME_MAX_LEN];
    /** Sensor firmware version. */
    char firmwareVersion[SENSOR_VERSION_MAX_LEN];
    /** Sensor hardware version. */
    char hardwareVersion[SENSOR_VERSION_MAX_LEN];
    /**Sensor type ID (described in {@link SensorTypeTag}). */
    int32_t sensorTypeId;
    /** Sensor ID, defined by sensor driver developers. */
    int32_t sensorId;
    /** Maximum measurement range of the sensor. */
    float maxRange;
    /** Sensor accuracy. */
    float accuracy;
    /** Sensor power. */
    float power;
};

/**
 * @brief Defines the data reported by sensors.
 *
 * The reported sensor data includes the sensor ID, sensor algorithm version, data generation time,
 * data options, data reporting mode, data address, and data length.
 *
 * @since 2.2
 */
struct SensorEvents {
    /** Sensor ID. */
    int32_t sensorId;
    /** Sensor algorithm version. */
    int32_t version;
    /** Time when sensor data is generated. */
    int64_t timestamp;
    /** Sensor data options, including the measurement range and accuracy. */
    uint32_t option;
    /** Sensor data reporting mode. */
    int32_t mode;
    /** Sensor data address. */
    uint8_t *data;
    /** Sensor data length. */
    uint32_t dataLen;
};

/**
 * @brief Defines the callback for reporting sensor data. A sensor user must register this callback
 * when subscribing to sensor data. The subscriber can receive
 * sensor data only after the sensor is enabled. For details, see {@link SensorInterface}.
 * 
 *
 * @since 2.2
 */
typedef int32_t (*RecordDataCallback)(const struct SensorEvents*);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif /* SENSOR_TYPE_H */
/** @} */
