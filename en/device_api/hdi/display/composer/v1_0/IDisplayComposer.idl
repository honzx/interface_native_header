/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Display
 * @{
 *
 * @brief Defines driver interfaces of the display module.
 *
 * This module provides driver interfaces for upper-layer graphics services, including layer management, device control, and display buffer management.
 *
 * @since 3.2
 * @version 1.0
 */

 /**
 * @file IDisplayComposer.idl
 *
 * @brief Declares the display composer interfaces.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Defines the package path of the display module interfaces.
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.display.composer.v1_0;

import ohos.hdi.display.composer.v1_0.DisplayComposerType;
import ohos.hdi.display.composer.v1_0.IHotPlugCallback;
import ohos.hdi.display.composer.v1_0.IVBlankCallback;
import ohos.hdi.display.composer.v1_0.IRefreshCallback;

sequenceable OHOS.HDI.Display.HdifdParcelable;
sequenceable OHOS.HDI.Display.BufferHandleParcelable;

 /**
 * @brief Defines the class that provides the display composer interfaces.
 *
 * This class provides interfaces for registering hot plug event callbacks and obtaining display capabilities.
 *
 * @since 3.2
 * @version 1.0
 */

interface IDisplayComposer {
    /**
     * @brief Registers a callback to be invoked when a hot plug event occurs.
     *
     * When a hot plug event occurs, the interface implementation layer triggers the callback to notify the graphics services of the event.
     *
     * @param cb Indicates the callback of the hot plug event.
     *
     * @return Returns <b>DISPLAY_SUCCESS</b> if the operation is successful.
     * @return Returns an error code defined in {@link DispErrCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    RegHotPlugCallback([in] IHotPlugCallback cb);

    /**
     * @brief Obtains the capabilities of a display.
     *
     * 
     *
     * @param devId Indicates the ID of the display.
     * @param info Indicates the capabilities supported by the display. For details, see {@link DisplayCapability}.
     *
     * @return Returns <b>DISPLAY_SUCCESS</b> if the operation is successful.
     * @return Returns an error code defined in {@link DispErrCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    GetDisplayCapability([in] unsigned int devId, [out] struct DisplayCapability info);

    /**
     * @brief Obtains the display modes supported by a display.
     *
     * 
     *
     * @param devId Indicates the ID of the display.
     * @param modes Indicates the information about all modes supported by the display, including all supported resolutions and refresh rates. Each mode has an ID, which will be used when
     * the mode is set or obtained. For details, see {@link DisplayModeInfo}.
     *
     * @return Returns <b>DISPLAY_SUCCESS</b> if the operation is successful.
     * @return Returns an error code defined in {@link DispErrCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    GetDisplaySupportedModes([in] unsigned int devId, [out] struct DisplayModeInfo[] modes);

    /**
     * @brief Obtains the display mode of a display.
     *
     * The display mode is written by the interface implementation layer.
     *
     * @param devId Indicates the ID of the display.
     * @param modeId Indicates the display mode ID of the display.
     *
     * @return Returns <b>DISPLAY_SUCCESS</b> if the operation is successful.
     * @return Returns an error code defined in {@link DispErrCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    GetDisplayMode([in] unsigned int devId, [out] unsigned int modeId);

    /**
     * @brief Sets a display mode for a display.
     *
     * 
     *
     * @param devId Indicates the ID of the display.
     * @param modeId Indicates the ID of the display mode. The interface implementation layer will switch the display to the specified display mode.
     *
     * @return Returns <b>DISPLAY_SUCCESS</b> if the operation is successful.
     * @return Returns an error code defined in {@link DispErrCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    SetDisplayMode([in] unsigned int devId, [in] unsigned int modeId);

    /**
     * @brief Obtains the power status of a display.
     *
     * The power status is written by the interface implementation layer.
     *
     * @param devId Indicates the ID of the display.
     * @param status Indicates the power status. For details, see {@link DispPowerStatus}.
     *
     * @return Returns <b>DISPLAY_SUCCESS</b> if the operation is successful.
     * @return Returns an error code defined in {@link DispErrCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    GetDisplayPowerStatus([in] unsigned int devId, [out] enum DispPowerStatus status);

    /**
     * @brief Sets a power status for a display.
     *
     *
     *
     * @param devId Indicates the ID of the display.
     * @param status Indicates the power status to set. For details, see {@link DispPowerStatus}.
     *
     * @return Returns <b>DISPLAY_SUCCESS</b> if the operation is successful.
     * @return Returns an error code defined in {@link DispErrCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    SetDisplayPowerStatus([in] unsigned int devId, [in] enum DispPowerStatus status);

    /**
     * @brief Obtains the backlight value of a display.
     *
     * The backlight value is written by the interface implementation layer.
     *
     * @param devId Indicates the ID of the display.
     * @param level Indicates the backlight value.
     *
     * @return Returns <b>DISPLAY_SUCCESS</b> if the operation is successful.
     * @return Returns an error code defined in {@link DispErrCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    GetDisplayBacklight([in] unsigned int devId, [out] unsigned int level);

    /**
     * @brief Sets a backlight value for a display.
     *
     *
     *
     * @param devId Indicates the ID of the display.
     * @param level Indicates the backlight value to set, ranging from 0 to 255.
     *
     * @return Returns <b>DISPLAY_SUCCESS</b> if the operation is successful.
     * @return Returns an error code defined in {@link DispErrCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    SetDisplayBacklight([in] unsigned int devId, [in] unsigned int level);

    /**
     * @brief Obtains the property value of a display.
     *
     * The property value is written by the interface implementation layer. 
     *
     * @param devId Indicates the ID of the display.
     * @param id Indicates the property ID returned by calling {@link GetDisplayCapability}.
     * @param value Indicates the property value corresponding to the property ID.
     *
     * @return Returns <b>DISPLAY_SUCCESS</b> if the operation is successful.
     * @return Returns an error code defined in {@link DispErrCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    GetDisplayProperty([in] unsigned int devId, [in] unsigned int id, [out] unsigned long value);

    /**
     * @brief Obtains the layers whose composition types have changed.
     *
     * In the preparation for composition, the display changes the composition type for each layer based on its own composition capability. This interface returns the layers whose composition types have changed.
     *
     * @param devId Indicates the ID of the display.
     * @param layers Indicates the start address of the layer array.
     * @param type Indicates the start address of the composition type array.
     *
     * @return Returns <b>DISPLAY_SUCCESS</b> if the operation is successful.
     * @return Returns an error code defined in {@link DispErrCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    GetDisplayCompChange([in] unsigned int devId, [out] unsigned int[] layers, [out] int[] type);

    /**
     * @brief Sets a cropped region for a display.
     *
     * You can use this interface to set the cropped region of the client buffer of the display. The cropped region cannot exceed the size of the client buffer.
     *
     * @param devId Indicates the ID of the display.
     * @param rect Indicates the cropped region of the client buffer.
     *
     * @return Returns <b>DISPLAY_SUCCESS</b> if the operation is successful.
     * @return Returns an error code defined in {@link DispErrCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    SetDisplayClientCrop([in] unsigned int devId, [in] struct IRect rect);

    /**
     * @brief Sets a display region for a display.
     *
     * 
     *
     * @param devId Indicates the ID of the display.
     * @param rect Indicates the display region to set.
     *
     * @return Returns <b>DISPLAY_SUCCESS</b> if the operation is successful.
     * @return Returns an error code defined in {@link DispErrCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    SetDisplayClientDestRect([in] unsigned int devId, [in] struct IRect rect);

    /**
     * @brief Enables or disables the vertical sync signal.
     *
     * When the vertical sync signal is generated, the {@link VBlankCallback} callback registered by {@link RegDisplayVBlankCallback}
     * will be invoked.
     * The vertical sync signal must be enabled when the graphics service needs to refresh the display, and disabled when display refresh is not required. The graphics service composes layers and sends the composition result to the device for display. 
     * @param devId Indicates the ID of the display.
     * @param enabled Specifies whether to enable or disable the vertical sync signal. The value <b>true</b> means to enable the vertical sync signal, and <b>false</b> means the opposite.
     *
     * @return Returns <b>DISPLAY_SUCCESS</b> if the operation is successful.
     * @return Returns an error code defined in {@link DispErrCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    SetDisplayVsyncEnabled([in] unsigned int devId, [in] boolean enabled);

    /**
     * @brief Registers a callback to be invoked when a VBLANK event occurs.
     *
     *
     *
     * @param devId Indicates the ID of the display.
     * @param cb Indicates the callback used to notify the graphics service of the VBLANK event occurred when <b>DisplayVsync</b> is enabled.
     *
     * @return Returns <b>DISPLAY_SUCCESS</b> if the operation is successful.
     * @return Returns an error code defined in {@link DispErrCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    RegDisplayVBlankCallback([in] unsigned int devId, [in] IVBlankCallback cb);

    /**
     * @brief Obtains the fences of the display layers after the commit operation.
     *
     * 
     *
     * @param devId Indicates the ID of the display.
     * @param layers Indicates the start address of the layer array.
     * @param fences Indicates the start address of the fence array.
     *
     * @return Returns <b>DISPLAY_SUCCESS</b> if the operation is successful.
     * @return Returns an error code defined in {@link DispErrCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    GetDisplayReleaseFence([in] unsigned int devId, [out] unsigned int[] layers, [out] HdifdParcelable[] fences);

    /**
     * @brief Creates a virtual display.
     *
     * 
     *
     * @param width Indicates the pixel width of the display.
     * @param height Indicates the pixel height of the display.
     * @param format Indicates the pixel format of the display.
     * For details, see {@link PixelFormat}. The <b>format</b> can be modified based on hardware requirements and returned to the graphics service.
     * @param devId Indicates the ID of the virtual display created.
     *
     * @return Returns <b>DISPLAY_SUCCESS</b> if the operation is successful.
     * @return Returns an error code defined in {@link DispErrCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    CreateVirtualDisplay([in] unsigned int width, [in] unsigned int height, [out] int format, [out] unsigned int devId);

    /**
     * @brief Destroys a virtual display.
     *
     *
     *
     * @param devId Indicates the ID of the display.
     *
     * @return Returns <b>DISPLAY_SUCCESS</b> if the operation is successful.
     * @return Returns an error code defined in {@link DispErrCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    DestroyVirtualDisplay([in] unsigned int devId);

    /**
     * @brief Sets the output buffer for a virtual display.
     *
     * This buffer stores the output of the virtual display. It can be used only after the sync fence is in the signaled state.
     *
     * @param devId Indicates the ID of the display.
     * @param buffer Indicates the output buffer.
     * @param fence Indicates the sync fence.
     *
     * @return Returns <b>DISPLAY_SUCCESS</b> if the operation is successful.
     * @return Returns an error code defined in {@link DispErrCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    SetVirtualDisplayBuffer([in] unsigned int devId, [in] BufferHandleParcelable buffer, [in] HdifdParcelable fence);

    /**
     * @brief Sets a property for a display.
     *
     * 
     *
     * @param devId Indicates the ID of the display.
     * @param id Indicates the property ID returned by calling {@link GetDisplayCapability}.
     * @param value Indicates the property value.
     *
     * @return Returns <b>DISPLAY_SUCCESS</b> if the operation is successful.
     * @return Returns an error code defined in {@link DispErrCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    SetDisplayProperty([in] unsigned int devId, [in] unsigned int id, [in] unsigned long value);

    /**
     * @brief Opens a layer on a specified display.
     *
     * Before using a layer on the GUI, you must open the layer based on the layer information. After the layer is opened, you can obtain the layer ID and then use other interfaces based on the layer ID.
     *
     * @param devId Indicates the ID of the display. A maximum of five displays are supported. The value ranges from 0 to 4, where <b>0</b> indicates the first display and <b>4</b> indicates the last display.
     * @param layerInfo Indicates the layer information passed to open a layer, including the layer type, layer size, and pixel format.
     * @param layerId Indicates the layer ID, which identifies a unique layer. The layer ID is returned to the GUI after the layer is opened.
     *
     * @return Returns <b>DISPLAY_SUCCESS</b> if the operation is successful.
     * @return Returns an error code defined in {@link DispErrCode} if the operation fails.
     *
     * @see CloseLayer
     * @since 3.2
     * @version 1.0
     */
    CreateLayer([in] unsigned int devId, [in] struct LayerInfo layerInfo, [out] unsigned int layerId);
    DestroyLayer([in] unsigned int devId, [in] unsigned int layerId);
    /* Function for SMQ transfer */
    InitCmdRequest([in] SharedMemQueue<int> request);
    CmdRequest([in] unsigned int inEleCnt, [in] struct HdifdInfo[] inFds, [out] unsigned int outEleCnt,
        [out] struct HdifdInfo[] outFds);
    GetCmdReply([out] SharedMemQueue<int> reply);
}
/** @} */
