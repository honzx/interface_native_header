/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup USB
 * @{
 *
 * @brief Defines the standard APIs of the USB function.
 *
 * This module declares the custom data types and functions used to obtain descriptors, interface objects, and request objects, and to submit requests.
 *
 * @since 3.0
 * @version 1.0
 */

/**
 * @file usb_info.h
 *
 * @brief Declares the data types used by the USB driver service subscriber.
 *
 * @since 3.0
 * @version 1.0
 */

#ifndef USB_INFO_H
#define USB_INFO_H

#include <string>
#include <vector>

namespace OHOS {
namespace USB {
/**
 * @brief Defines the USB device information.
 *
 */
struct USBDeviceInfo {
    /** USB device status */
    int32_t status;
    /** USB bus ID */
    int32_t busNum;
    /** USB device ID */
    int32_t devNum;
};

/**
 * @brief Defines the USB device information.
 *
 */
class UsbInfo {
public:
    /**
     * @brief Sets USB device status.
     *
     * @param status USB device status.
     *
     * @since 3.0
     * @version 1.0
     */
    void setDevInfoStatus(int32_t status)
    {
        devInfo.status = status;
    }

    /**
     * @brief Sets the USB bus ID.
     *
     * @param busNum USB bus ID.
     *
     * @since 3.0
     * @version 1.0
     */
    void setDevInfoBusNum(int32_t busNum)
    {
        devInfo.busNum = busNum;
    }

    /**
     * @brief Sets the USB device ID.
     *
     * @param devNum USB device ID.
     *
     * @since 3.0
     * @version 1.0
     */
    void setDevInfoDevNum(int32_t devNum)
    {
        devInfo.devNum = devNum;
    }

    /**
      * @brief Obtains the USB device status.
     *
     * @param status USB device status.
     *
     * @since 3.0
     * @version 1.0
     */
    int32_t getDevInfoStatus() const
    {
        return devInfo.status;
    }

    /**
     * @brief Obtains the USB bus ID.
     *
     * @param busNum USB bus ID.
     *
     * @since 3.0
     * @version 1.0
     */
    int32_t getDevInfoBusNum() const
    {
        return devInfo.busNum;
    }

    /**
     * @brief Obtains the USB device ID.
     *
     * @param devNum USB device ID.
     *
     * @since 3.0
     * @version 1.0
     */
    int32_t getDevInfoDevNum() const
    {
        return devInfo.devNum;
    }

private:
    /** USB device information */
    USBDeviceInfo devInfo;
};
} /** namespace USB */
} /** namespace OHOS */

#endif /** USBMGR_USB_INFO_H */
